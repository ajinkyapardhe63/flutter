import 'package:flutter/material.dart';
import 'Animal.dart';
import 'Jawan.dart';
import 'Tiger3.dart';
import 'Peaky_blinders.dart';
import 'Wednesday.dart';
import '1899.dart';
import 'Haunting.dart';
import 'Nun2.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: const [Icon(Icons.search_rounded, color: Colors.red)],
        title: const Text("NETFLIX",
            style: TextStyle(
                fontStyle: FontStyle.normal, fontSize: 30, color: Colors.red)),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Container(
         decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.black,Colors.black,Colors.red],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            ),
         ),
        child: SingleChildScrollView(
          child: Column(children: [
            Column(
              children: [
                const Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Movies",
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          const Image(
                            image: AssetImage('assets/jawan.jpg'),
                            width: 200,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            child: const Text(
                              "Jawan",
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: ((context) => const Jawan())));
                            },
                          )
                        ],
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/Animal.jpg",
                            width: 200,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            child:
                          const Text(
                            "Animal",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Animal())));
                          }
                          ),
                      ],
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        children: [
                          Image.asset(
                            "assets/tigerposter.jpg",
                            width: 200,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                         GestureDetector(
                            child:
                          const Text(
                            "Tiger3",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Tiger3())));
                          }
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              children: [
                Column(
                  children: [
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Series",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(children: [
                            Image.asset(
                              'assets/peakyblinders.jpg',
                              width: 200,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                             GestureDetector(
                            child:
                          const Text(
                            "Peaky Blinders",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Peaky_blinders())));
                          }
                          ),
                          ]),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/wednesday.jpg",
                                width: 200,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                               GestureDetector(
                            child:
                          const Text(
                            "Wednesday",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Wednesday())));
                          }
                          ),
                            ],
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/hauntinginvenice.jpg",
                                width: 200,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                               GestureDetector(
                            child:
                          const Text(
                            "A Haunting in Venice ",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Haunting())));
                          }
                          ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Column(
              children: [
                Column(
                  children: [
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Most Popular",
                        style: TextStyle(fontSize: 20, color: Colors.white),
                        textAlign: TextAlign.start,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(children: [
                            Image.asset(
                              'assets/nun2.jpg',
                              width: 200,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                             GestureDetector(
                            child:
                          const Text(
                            "The Nun II",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Nun2())));
                          }
                          ),
                          ]),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/1899.jpg",
                                width: 200,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                               GestureDetector(
                            child:
                          const Text(
                            "1899",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const E1899())));
                          }
                          ),
                            ],
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            children: [
                              Image.asset(
                                "assets/thisistheway.png",
                                width: 200,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                               GestureDetector(
                            child:
                          const Text(
                            "This is the Way",
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onTap: () {
                            
                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: ((context)=>const Tiger3())));
                          }
                          ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ]),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: Icon(
              Icons.home,
              color: Color.fromARGB(255, 104, 7, 0),
              size: 40,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(
                Icons.search,
               color: Color.fromARGB(255, 104, 7, 0),
                size: 40,
              ),
              label: 'Search',
              ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add,
              color: Color.fromARGB(255, 104, 7, 0),
              size: 40,
            ),
            label: 'Favorites',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.favorite,
              color: Color.fromARGB(255, 104, 7, 0),
              size: 35,
            ),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
             color: Color.fromARGB(255, 104, 7, 0),
              size: 40,
            ),
            label: 'Profile',
          ),
        ],
      ),
    );
  }
}
