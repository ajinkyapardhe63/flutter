import 'package:flutter/material.dart';
import 'homepage.dart';
class Haunting extends StatelessWidget{
  const Haunting({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("A Haunting in Venice",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/hauntingbg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black45, BlendMode.darken),
        )),
        child:
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
        
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Series Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/hauntings1.jpg",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/hauntings2.jpg",
                        width: 400,
                      ),
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/hauntings3.jpg",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/hauntings4.jpg",
                        width: 400,
                      ),
                     
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    
                  ],
                ),
              ),
              const Text(
                "Series Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              
              const Text(
                "Kyle Allen, Camille Cottin, Jamie Dornan, Tina Fey, Jude Hill, Ali Khan, Emma Laird, Kelly Reilly, Riccardo Scamarcio, and Michelle Yeoh.",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Series Description",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "In 1947, Hercule Poirot has retired to Venice, having lost his faith in God and humanity, with ex-police officer Vitale Portfoglio as his bodyguard. Mystery writer Ariadne Oliver persuades Poirot to attend a Halloween party and séance at the palazzo of famed opera singer Rowena Drake, wishing to expose Joyce Reynolds—a World War I army nurse turned medium—as a fraud. The palazzo, a former orphanage, is believed to be haunted by the spirits of orphaned children who were locked up and abandoned to die there during a city-wide plague; rumors claim that the spirits torment any nurses and doctors who dare enter.Rowena has hired Joyce to commune with her daughter Alicia, who committed suicide after Alicia's fiancé, chef Maxime Gerard, ended their engagement. Among the guests are Rowena's housekeeper Olga Seminoff, Drake family doctor Leslie Ferrier and his son Leopold, and Joyce's Romani assistant Desdemona Holland; they are joined by Maxime right before the séance, and during it Poirot reveals Desdemona's half-brother Nicholas—and Joyce's second assistant—hiding in the chimney. Joyce suddenly speaks in Alicia's voice, saying that one of the guests murdered her. Poirot confronts Joyce, who insists he lighten up, gives him her mask and robe, and cryptically says they will not meet again. Seconds later, an unknown assailant nearly drowns Poirot when he is apple bobbing, while Joyce falls from an upper story and is impaled on a courtyard statue.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
        ),
      ),
    );
  }
}