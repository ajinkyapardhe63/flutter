import 'package:flutter/material.dart';
import 'homepage.dart';

class Jawan extends StatelessWidget {
  const Jawan({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const Homepage()));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        backgroundColor: Colors.black,
        title: const Text(
          "Jawan",
          style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/jawan.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Movie Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/jawans2.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/jawans4.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/jawans3.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/jawans1.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/jawans5.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/jawans6.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                  ],
                ),
              ),
              const Text(
                "Movie Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Hero - Shah Rukh Khan,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Heroin-Deepika Padukone,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Supporting Roles - Nayanthara,Vijay setupati,priyamani,Sanya Malhotra",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Movie Description",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Azad is the jailer of a women's prison in Mumbai who hijacks a Mumbai Metro train with a group of six inmates: Lakshmi, Eeram, Ishkra, Kalki, Helena and Janhvi. He negotiates with NSG officer Narmada Rai to ask the Agriculture Minister to send ₹40,000 crore in exchange for the passengers' lives. Kalee Gaikwad, a global arms dealer, learns that his daughter Alia is one of the captives and agrees to fund the deal. Azad plans to donate that money toward the loan waiver of 700,000 impoverished farmers in the country. Azad and his gang transfer the funds into the farmers' bank accounts and escape. Through Alia, he reveals his name as Vikram Rathore to Kalee.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
      ),
    );
  }
}
