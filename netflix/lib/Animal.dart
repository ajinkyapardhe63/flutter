import 'package:flutter/material.dart';
import 'homepage.dart';
class Animal extends StatelessWidget{
  const Animal({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("Animal",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/Animalbg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Movie Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/Animals1.jpg",
                        width: 400,
                      ),
                     
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/Animals2.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/Animals3.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/Animals4.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/Animals5.jpg",
                        width: 400,
                      ),
                     
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/Animals6.jpg",
                        width: 400,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                  ],
                ),
              ),
              const Text(
                "Movie Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Hero - Ranbir Kapoor,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Heroin- Rashmika Mandana,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Supporting Roles - Anil Kapoor,Bobby Deol,Saloni batra,Anshhul Chauhan",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Movie Description",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Ranvijay 'Vijay' Singh is the son of Balbir Singh, a Delhi business tycoon. His love for his father is exceptional though Balbir's schedule keeps him from spending time with his family. One day, Balbir ousts Vijay to boarding school in the USA when Vijay threatens his sister Reet's bullies with an AK-47. Vijay finishes his education and returns home after several years. During Balbir's birthday party, he gets into an argument with Reet's husband Varun, prompting Balbir to expel him from the house again. Vijay marries his childhood love Geetanjali, and the couple cuts off their families and moves to the USA.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
      ),
    );
  }
}
