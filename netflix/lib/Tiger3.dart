import 'package:flutter/material.dart';
import 'homepage.dart';
class Tiger3 extends StatelessWidget{
  const Tiger3({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("Tiger 3",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/tiger3bg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Movie Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s1.jpg",
                        width: 370,
                      ),
                     
                    ]),
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s2.jpg",
                        width: 370,
                      ),
                      
                    ]),
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s3.jpg",
                        width: 370,
                      ),
                     
                    ]),
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s4.jpg",
                        width: 370,
                      ),
                      
                    ]),
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s5.jpg",
                        width: 370,
                      ),
                      
                    ]),
                    Column(children: [
                      Image.asset(
                        "assets/tiger3s6.jpg",
                        width: 370,
                      ),
                      
                    ]),
                  ],
                ),
              ),
              const Text(
                "Movie Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Hero - Salman Khan,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Heroin- Katrina Kaif,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Supporting Roles - Emraan Hashmi,Shah Rukh Khan",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Movie Description",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              const Text(
                "In 1999, a teenage Zoya witnesses her father, ISI agent Rehan Nazar, die in a bomb blast. After his funeral, she is visited by Nazar's protege, Aatish Rehman, who convinces her to join the ISI.In the present, Avinash 'Tiger' Singh Rathore is called for a mission by the new RAW chief Maithili Menon to rescue Tiger's former handler Gopi Arya, who was captured in Afghanistan after trying to extract information from the Taliban about a mission being planned in Pakistan. Following his rescue, Gopi reveals to Tiger that Zoya is involved in the planned mission and succumbs to his injuries..",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
      ),
    );
  }
}