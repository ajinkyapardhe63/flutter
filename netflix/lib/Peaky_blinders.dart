import 'package:flutter/material.dart';
import 'homepage.dart';
// ignore: camel_case_types
class Peaky_blinders extends StatelessWidget{
  const Peaky_blinders ({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("Peaky Blinders",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/peakybg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken),
        )),
        child: SingleChildScrollView(
          scrollDirection:Axis.vertical,
          child:
        Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Series Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/peakys1.jpg",
                        width: 318,
                      ),
                      
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/pbs2.jpg",
                        width: 400,
                      ),
                     
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/pbs3.jpg",
                        width: 400,
                      ),
                     
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/pbs4.jpg",
                        width: 400,
                      ),
                     
                    ]),
                    const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/pbs5.jpg",
                        width: 416,
                      ),
                      
                    ]),
                    
                  ],
                ),
              ),
              const Text(
                "Series Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                " Cillian Murphy, Helen McCrory, Paul Anderson, Sophie Rundle , Joe Cole,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
             
              
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Series Description",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.left,
              ),
              
              const Text(
                "Peaky Blinders is a crime drama centred on a family of mixed Irish Traveller and Romani origins based in Birmingham, England, starting in 1919, several months after the end of the First World War. It centres on the Peaky Blinders street gang and their ambitious, cunning crime boss Tommy Shelby. The gang comes to the attention of Major Chester Campbell, a detective chief inspector in the Royal Irish Constabulary sent over by Winston Churchill from Belfast, where he had been sent to clean up the city of the Irish Republican Army flying columns, the Communist Party of Great Britain, street gangs, and common criminals.[1][2] Winston Churchill (played by Andy Nyman in series 1 and Richard McCabe in series 2) charges him with suppressing disorder and uprising in Birmingham and recovering a stolen cache of arms and ammunition meant to be shipped to Libya.[3][4] The first series concludes on 3 December 1919 — 'Black Star Day', the event where the Peaky Blinders plan to take over Billy Kimber's betting pitches at the Worcester Races.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              
            ]),),
      ),
    );
  }
}