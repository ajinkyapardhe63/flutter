import "dart:async";
import "package:flutter/material.dart";
import "homepage.dart";

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 8), () {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const Homepage(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [Colors.black, Colors.black, Colors.red],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        )),
        child: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Image.asset(
              "assets/N.png",
              height: 100,
              width: 100,
            ),
            const Center(
              child: Text(
                "Netflix",
                style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 50,
                    fontWeight: FontWeight.w500,
                    color: Colors.red),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
