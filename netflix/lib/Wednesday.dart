import 'package:flutter/material.dart';
import 'homepage.dart';
class Wednesday extends StatelessWidget{
  const Wednesday({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("Wednesday",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/Wednesdaybg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black45, BlendMode.darken),
        )),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Series Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/wednesdays1.png",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/wednesdays2.png",
                        width: 400,
                      ),
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/wednesdays3.png",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/wednesdays4.png",
                        width: 400,
                      ),
                     
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    
                  ],
                ),
              ),
              const Text(
                "Series Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              
              const Text(
                " Jenna Ortega, Gwendoline Christie, Riki Lindhome,Jamie McShane, Percy Hynes White,",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Series Description",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "Wednesday Addams is expelled from her school after dumping live piranhas into the school's pool in retaliation for the boys' water polo team bullying her brother Pugsley. Consequently, her parents Gomez and Morticia Addams enroll her at their high school alma mater Nevermore Academy, a private school for monstrous outcasts, in the town of Jericho, Vermont. Wednesday's cold, emotionless personality and her defiant nature make it difficult for her to connect with her schoolmates and cause her to run afoul of the school's principal Larissa Weems. However, she discovers she has inherited her mother's psychic abilities, which allow her to solve a local murder mystery.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
      ),
    );
  }
}