import 'package:flutter/material.dart';
import 'homepage.dart';
class E1899 extends StatelessWidget{
  const E1899({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
       leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder:((context) => const Homepage() )));
          },
          child: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
        centerTitle: true,
        title: const Text("1899",style: TextStyle(color:Colors.red,fontWeight:FontWeight.bold)),
        backgroundColor: Colors.black,

      ),
       body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/1899bg.jpg"),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black45, BlendMode.darken),
        )),
        child:
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
        
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Movies Scence",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(children: [
                      Image.asset(
                        "assets/1899s3.jpeg",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/1899s4.jpg",
                        width: 450,
                      ),
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/1899s1.jpg",
                        width: 400,
                      ),
                     
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                    Column(children: [
                      Image.asset(
                        "assets/1899s2.png",
                        width: 460,
                      ),
                     
                      
                    ]),
                     const SizedBox(
                        width: 10,
                      ),
                     Column(children: [
                      Image.asset(
                        "assets/1899s5.jpg",
                        width: 390,
                      ),
                     
                      
                    ]),
                  ],
                ),
              ),
              const Text(
                "Movies Cast",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              
              const Text(
                "Taissa Farmiga, Jonas Bloquet, Bonnie Aarons, Storm Reid,Anna Popplewell.",
                style: TextStyle(fontSize: 20, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Movies Description",
                style: TextStyle(fontSize: 20, color: Colors.white,fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              const Text(
                "In Tarascon, Irene is haunted by Valak and has a strange vision. Sister Debra tells Irene that she received Noiret's rosary from Jacques. At school, Sophie is bullied by classmates and locked in the deconsecrated sealed-off chapel. The bully points out the goat on the stained-glass window, claiming the devil appears when the sun shines through and turns the goat's eyes red. One night, the headmistress encounters a sleepwalking Maurice and is killed by Valak in the guise of her deceased son.Irene and Debra travel to the Palais des Papes and meet with a librarian. He explains that Valak was an angel rejected by God and the emblem on Noiret's rosary is the family crest of St. Lucy, who was martyred by a pagan. Though she was set on fire, she miraculously did not burn; her eyes were gouged out, but her family recovered them. The librarian suggests the demon is killing St. Lucy's descendants because it wants this powerful relic, last known to be stored in a former monastery-turned-winery. The winery then became the present-day boarding school.",
                style: TextStyle(fontSize: 18, color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ]),
        ),
      ),
    );
  }
}