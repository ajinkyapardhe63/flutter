import 'package:flutter/material.dart';
import"package:provider/provider.dart";

void main(){
  runApp(const MyApp());

}
class MyApp extends StatelessWidget{
  const MyApp({super.key});
  @override
  Widget build(BuildContext context){
    return Provider(
      create:(context) {
        return Company(compName:"Google",empcount:250);
      },
      child:const MaterialApp(
        debugShowCheckedModeBanner: false,
        home:MainApp(),
      ) ,
      );
  }
}
class MainApp extends StatefulWidget{
  const MainApp({super.key});
  @override
  State createState()=>_MainAppState();
}
class _MainAppState extends State{
  @override
  Widget build(BuildContext context){
    return  Scaffold(
      appBar: AppBar(
        backgroundColor:Colors.amber ,
        title: const Text("Provider State Management"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(Provider.of<Company>(context).compName),
            const SizedBox(
              height: 20,
            ),
            Text("${Provider.of<Company>(context).empcount}"),
            const SizedBox(
              height: 10,
            ),
            
          ],
          ),
      )
    );
  }
}
class Company{
  String compName;
  int empcount;
  Company({required this.compName,required this.empcount});
}