import'package:flutter/material.dart';

class Assignment4 extends StatefulWidget{
  const Assignment4({super.key});
  @override
  State<Assignment4> createState()=>_Assignment4State();
}
class _Assignment4State extends State<Assignment4>{
  bool _like1 =false;
  bool _like2=false;
  bool _like3=false;
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Instagram",style: TextStyle(fontStyle: FontStyle.italic,color: Colors.white,fontSize: 30,),),
        backgroundColor: Colors.purple,
        actions: const [
          Icon(
            Icons.favorite_rounded,
            color: Colors.red,
          )
        ],
        
      ),
      /*body: ListView( // by default listviwe is scrollable    
        children: [
          Column( // Note column does not have default scroll therefore we use ListViwe
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color: Colors.yellow,child: Image.network("https://images.unsplash.com/photo-1503614472-8c93d56e92ce?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8fA%3D%3D",width:double.infinity,)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                ],
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color:Colors.red,child: Image.network("https://images.pexels.com/photos/147411/italy-mountains-dawn-daybreak-147411.jpeg?cs=srgb&dl=pexels-pixabay-147411.jpg&fm=jpg",width:double.infinity,)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                ],
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color: Colors.blue,child: Image.network("https://images.unsplash.com/photo-1500964757637-c85e8a162699?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8YmVhdXRpZnVsJTIwbGFuZHNjYXBlfGVufDB8fDB8fHww",width:double.infinity,)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){}, 
                    icon: const Icon(
                      Icons.favorite_outline_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                ],
              )
            ],
          )

        ],
      ),
     */ body:SingleChildScrollView( //we use SingleChildScrollView for overloaded content we can scroll the screen
        child:Column(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color: Colors.yellow,child: Image.network("https://images.unsplash.com/photo-1503614472-8c93d56e92ce?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8fA%3D%3D",width:double.infinity,height: 250,)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        _like1=!_like1;
                      });
                    }, 
                    icon: _like1?
                    const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    )
                     :const Icon(
                      Icons.favorite_outline_rounded,
                    ),
                  ),
                  
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  /*const SizedBox(
                    width: 215,
                  ),
                  */
                  const Spacer(),
                  IconButton(onPressed:(){}, 
                  icon: const Icon(
                    Icons.bookmarks_outlined,
                  ),)
                ],
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color:Colors.red,child: Image.network("https://images.pexels.com/photos/147411/italy-mountains-dawn-daybreak-147411.jpeg?cs=srgb&dl=pexels-pixabay-147411.jpg&fm=jpg",width:double.infinity,height:220)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        _like2=!_like2;
                      });
                    },
                    icon :_like2?
                    const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    ) 
                    : const Icon(
                      Icons.favorite_outline_rounded,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  /*const SizedBox(
                    width: 215,
                  ),*/
                  const Spacer(),
                  IconButton(onPressed:(){}, 
                  icon: const Icon(
                    Icons.bookmarks_outlined,
                  ),)
                ],
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(color: Colors.blue,child: Image.network("https://images.unsplash.com/photo-1500964757637-c85e8a162699?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8YmVhdXRpZnVsJTIwbGFuZHNjYXBlfGVufDB8fDB8fHww",width:double.infinity,height:220)),
              Row(
                children: [
                  IconButton(
                    onPressed: (){
                      setState(() {
                        _like3=!_like3;
                      });
                    },
                    icon :_like3?
                    const Icon(
                      Icons.favorite_rounded,
                      color: Colors.red,
                    ) 
                    :const Icon(
                      Icons.favorite_outline_rounded,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.comment_outlined,
                    ),
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: const Icon(
                      Icons.send,
                    ),
                  ),
                  /*const SizedBox(
                    width: 215,
                  ),
                  */
                  const Spacer(), // give space dynamically
                  IconButton(onPressed:(){}, 
                  icon: const Icon(
                    Icons.bookmarks_outlined,
                  ),)
                ],
              )
            ],
          )
        ],
        
      ),
      ),

    );
  }
}