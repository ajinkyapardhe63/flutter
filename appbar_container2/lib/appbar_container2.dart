import 'package:flutter/material.dart';
class appbar_container extends StatelessWidget{
  const appbar_container({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle:true,
        title: const Text("Appbar"),
        backgroundColor: Colors.red,
        actions: [
          IconButton(
            icon:const Icon(
              Icons.search
              ), 
              onPressed: (){}
              ),
              IconButton(icon:const Icon(Icons.favorite_rounded),onPressed:(){})
        ],

        ),
    );
    }
}