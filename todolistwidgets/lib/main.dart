import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("widget1"),
        ),
        body:Center(
          child:
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient:const  LinearGradient(colors: [Colors.purple,Colors.blue],
              stops:[0.3,1.0],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight
              ),
              boxShadow:const [
                BoxShadow(blurRadius:10,offset: Offset(20,20),color: Colors.black)
              ],
              border:Border.all(width:3,color: Colors.black)
            ),
            //child:Image.network("https://png.pngtree.com/png-vector/20210301/ourmid/pngtree-100-guaranteed-label-icon-vector-template-png-image_2985541.png",height: 100,width: 100,)
          )
        )
      )
    );
  }
}
