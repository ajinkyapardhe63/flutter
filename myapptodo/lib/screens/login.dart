import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Login extends StatefulWidget {
  const Login({super.key});
  @override
  State createState() => _Login();
}

class _Login extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.blue,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  
                  child: SvgPicture.asset(
                    "lib/assets/logimg.svg",height: 200,width: 300,
                      
                  )
                  )
            ]),
      ),
    ));
  }
}
