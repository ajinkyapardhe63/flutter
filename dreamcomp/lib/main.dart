import 'package:flutter/material.dart';

void main() {
  runApp(const DreamCompApp());
}

class DreamCompApp extends StatefulWidget {
  const DreamCompApp({super.key});

  State createState()=>DreamCompAppState();
}

class DreamCompAppState extends State{

  TextEditingController _nameStudent=TextEditingController();
  TextEditingController _nameCompany=TextEditingController();
  TextEditingController _locationCompany=TextEditingController();

  var name;
  var Company;
  var location;

  bool flag=false;

 void _printName(){

    name=_nameStudent.text;
    Company=_nameCompany.text;
    location=_locationCompany.text;

  }

  // void initState(){
  //   super.initState();
  //   _nameStudent.addListener(() { _printName();});
  // }

  void initState(){
    super.initState();
    _nameStudent.addListener(() { _printName();});
    _nameCompany.addListener(() {_printName();});
    _locationCompany.addListener(() {_printName(); });
  }


  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar:AppBar(
          title: const Text("Dream Company"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
        
              TextField(
                controller: _nameStudent ,
                decoration: InputDecoration(
                  hintText: "Enter Your Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
              ),
        
               const SizedBox(
                height: 20,
              ),
        
              TextField(
                controller: _nameCompany ,
                decoration: InputDecoration(
                  hintText: "Enter Company Name",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
              ),
        
               const SizedBox(
                height: 20,
              ),
        
              TextField(
                controller: _locationCompany ,
                decoration: InputDecoration(
                  hintText: "Enter Location",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
              ),
               const SizedBox(
                height: 30,
              ),
        
             ElevatedButton(onPressed: (){
        
        setState(() {
          
          flag=true;
        });
             }, child: const Text("Sumbit")),
        
          const SizedBox(
                height: 20,
              ),
        
        
            (flag)?  Container(
                  height: 180,
                width: 400,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 255, 226, 145),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                       const  SizedBox(
                              height: 30,
                        ),
                        Text("Name:- $name",style: const TextStyle(fontSize: 25),),
        
                       const  SizedBox(
                          height: 20,
                        ),
        
                         Text("Company Name:- $Company",style: const TextStyle(fontSize: 25),),
        
                          const  SizedBox(
                          height: 20,
                        ),
        
                         Text("Location:- $location",style: const TextStyle(fontSize: 25),),
                      ],
                    ),
                  ],
                ),
              ):
        
               Container(
                height: 180,
                width: 300,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 255, 226, 145),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                              height: 30,
                        ),
                        Text("Name:-",style: TextStyle(fontSize: 25),),
        
                        SizedBox(
                          height: 20,
                        ),
        
                         Text("Company Name:-",style: TextStyle(fontSize: 25),),
        
                           SizedBox(
                          height: 20,
                        ),
        
                         Text("Location:-",style: TextStyle(fontSize: 25),),
                      ],
                    ),
                  ],
                ),
              ),
        
            ],
          ),
        ),
         ),
    );
  }
}