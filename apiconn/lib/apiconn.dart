import "dart:convert";
import "dart:developer";

import "package:flutter/material.dart";
import "package:http/http.dart" as http;

class Apiconn extends StatefulWidget {
  const Apiconn({super.key});
  @override
  State createState() => ViewEmpData();
}

class ViewEmpData extends State {
  List<dynamic>empData = [];
  String msg="";
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("API Bindeing"),
        backgroundColor: Colors.pink,
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: empData.length,
          itemBuilder: (context, index) {
            return Row(
              children: [
                Text(" Name : ${empData[index]['employee_name']},"),
                const SizedBox(width: 10),
                Text("Salary : ${empData[index]['employee_salary']},"),
                 const SizedBox(width: 10),
                Text("Age :${empData[index]['employee_age']}"),
                const SizedBox(width: 10),
                
              ],
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: getEmpdata,
        child: const Icon(Icons.add),
      ),
    );
  }

  void getEmpdata() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employees"); //declaring URl link of API
    http.Response reponse = await http.get(url); //wait until it get connected to api link and get back response
    log(reponse.body);
    var responseData = json.decode(reponse.body); //convert response.body
    setState(() {
      empData = responseData["data"];
      msg=responseData["message"];
    });
  }
}
