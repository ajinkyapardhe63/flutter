import "dart:developer";

import "package:provider/provider.dart";
import "package:flutter/material.dart";
import "company.dart";
import 'mainapp.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    log("in main app build");
    return ChangeNotifierProvider(
      create: (context) {
        return Company(compName: "Google", empcount: 250);
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MyApp(),
      ),
    );
  }
}
