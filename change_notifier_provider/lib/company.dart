
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Company extends ChangeNotifier{
  String compName;
  int empcount;
  Company({required this.compName,required this.empcount});
  void changeCompany( String compName,int empcount){
    this.compName=compName;
    this.empcount=empcount;
    notifyListeners();
  }
}
class NormalClass extends StatelessWidget{
  const NormalClass({super.key});
  @override
  Widget build(BuildContext context){
    log("In normal Class build");
    return const Text("Hello");
  }
}