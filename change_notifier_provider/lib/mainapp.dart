import 'dart:developer';
import 'package:provider/provider.dart';
import 'company.dart';
import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  
  const MyApp({super.key});
  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State {
  @override
  Widget build(BuildContext context) {
    log("in myApp build");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Change Notifier Provider"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(Provider.of<Company>(context).compName),
            const SizedBox(height: 20),
            Text("${Provider.of<Company>(context).empcount}"),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  Provider.of<Company>(context,listen: false).changeCompany("Meta", 500);
                }, 
                child:const Text("Change Company")
            ),
            const SizedBox(height: 20),
            const NormalClass(),
          ],
        ),
      ),
    );
  }
}
