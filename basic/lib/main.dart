import 'package:flutter/material.dart';
import 's1.dart';
import 's2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:  CounterPage2(),
      debugShowCheckedModeBanner: false,
    );
  }
}