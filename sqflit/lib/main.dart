
import'package:sqflite/sqflite.dart';
import'package:flutter/widgets.dart';
import'package:path/path.dart';

dynamic database;
class Player{
  final String name;
  final int jerno;
  final int runs;
  final double avg;

  Player({
    required this.name,
    required this.jerno,
    required this.runs,
    required this.avg,
  });
  Map<String,dynamic>PlayerMap(){
    return{
      'name':name,
      'jerno':jerno,
      'runs':runs,
      'avg':avg,
    };
  }
  @override
  String toString(){
    return '{name:$name,jerno:$jerno,runs:$runs,avg:$avg,}';
  }

}
Future insertPlayerData(Player obj)async{
  final localDB=await database;
  await localDB.insert(
    'Player',
    obj.PlayerMap(),
    conflictAlgorithm:ConflictAlgorithm.replace,
  );
}
Future<List<Player>>getPlayerData()async{
  final localDB=await database;
  List<Map<String,dynamic>> listPlayers=await localDB.query("Player");
  return List.generate(listPlayers.length, (i) {
    return Player(
      name: listPlayers[i]['name'],
      jerno:listPlayers[i]['jerno'],
      runs: listPlayers[i]['runs'],
      avg: listPlayers[i]['avg'],
    );
  });
}
void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  database=openDatabase(
    join(await getDatabasesPath(),"PlayerDB.db"),
    version: 1,
    onCreate: (db,version)async{
      await db.execute('''CREATE TABLE Player(
        name TEXT,
        jerno INTEGER PRIMARY KEY,
        runs INT,
        avg REAL)''');
    },
  );
  Player batsman1=Player(name: "Virat Kohli", jerno: 18, runs: 50000, avg: 60.32);
  insertPlayerData(batsman1);
   Player batsman2=Player(name: "Rohit Sharma", jerno: 45, runs: 70000, avg: 80.32);
  insertPlayerData(batsman2);
   Player batsman3=Player(name: "Shubman Gill", jerno: 77, runs: 30000, avg: 40.32);
  await insertPlayerData(batsman3);

  print(await getPlayerData());


}