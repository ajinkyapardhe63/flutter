import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});
  @override
  State createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  bool ispressed1 = true;
  bool ispressed2 = true;
  bool ispressed3 = true;
  List<bool> optionpressed = [true, true, true, true, true];
  List<String> questions = [
    "",
    "who invented flutter?",
    "what is Main Axis Alignment in Column?",
    "what is Cross Axis Alignment in Row?",
    "what is the latest version of flutter?",
    "which programming language is used to build fluter Apps?",
  ];
  List<String> option1 = [
    "",
    "A. Facebook",
    "A. Width",
    "A. Size",
    "A.3.16.12",
    "A. Python"
  ];
  List<String> option2 = [
    "",
    "B. Google",
    "B. Cross height",
    "B. Height",
    "B. 3.15.8",
    "B. Ruby"
  ];
  List<String> option3 = [
    "",
    "C. Microsoft",
    "C. Height",
    "C. Cross width",
    "C. 3.16.9",
    "C. Java"
  ];
  List<String> option4 = [
    "",
    "D. Amazon",
    "D. Size",
    "D. Width",
    "D. 3.16.20",
    "D. Dart"
  ];
  List<String> score = [
    "",
    "20% Score",
    "40% Score",
    "60% Score",
    "80% Score",
    "100% Score"
  ];

  int queno = 1;
  int marks = 0;
  Color btn1() {
    if (queno == 2 || queno == 4) {
      return Colors.red;
    }
    if (queno == 1 || queno == 3) {
      return Colors.red;
    }
    if (queno == 5) {
      return Colors.red;
    }
    return Colors.blue;
  }

  Color btn2() {
    if (queno == 2 || queno == 4) {
      return Colors.red;
    }
    if (queno == 1 || queno == 3) {
      return Colors.green;
    }
    if (queno == 5) {
      return Colors.red;
    }

    return Colors.blue;
  }

  Color btn3() {
    if (queno == 2 || queno == 4) {
      return Colors.green;
    }
    if (queno == 1 || queno == 3) {
      return Colors.red;
    }
    if (queno == 5) {
      return Colors.red;
    }

    return Colors.blue;
  }

  Color btn4() {
    if (queno == 2 || queno == 4) {
      return Colors.red;
    }
    if (queno == 1 || queno == 3) {
      return Colors.red;
    }
    if (queno == 5) {
      return Colors.green;
    }

    return Colors.blue;
  }

  String text() {
    if (queno < 5) {
      return "next";
    }
    if (queno == 5) {
      return "Submit";
    }
    if (queno == 6) {
      return "Restart";
    }
    return "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Quiz App"),
          backgroundColor:const Color.fromARGB(255, 222, 185, 255),
          elevation: 0,
          // shape:const RoundedRectangleBorder(
          //   borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
          // ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
                gradient: LinearGradient(
              colors: [Colors.purple, Colors.blue],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            )),
          ),
        ),
        body: Container(
          color: const Color.fromARGB(255, 222, 185, 255),
          child: Center(
              child: (queno < 6)
                  ? Column(
                      children: [
                        const SizedBox(height: 10),
                        SizedBox(
                            width: 500,
                            child: Text(
                              "Marks :$marks/5",
                              style: const TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w600),
                              textAlign: TextAlign.right,
                            )),
                        const SizedBox(height: 10),
                        Text(
                          "Question : $queno/5",
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                        const SizedBox(height: 40),
                        Text(
                          questions[queno],
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 40),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: const Size(300, 50),
                              backgroundColor:
                                  optionpressed[0] ? Colors.white : btn1(),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          onPressed: () {
                            setState(() {
                              if (queno == 1 || queno == 3) {
                                optionpressed[0] = false;
                                optionpressed[1] = false;
                              }
                              if (queno == 2 || queno == 4) {
                                optionpressed[0] = false;
                                optionpressed[2] = false;
                              }
                              if (queno == 5) {
                                optionpressed[0] = false;
                                optionpressed[3] = false;
                              }
                            });
                          },
                          child: Text(
                            option1[queno],
                            style: const TextStyle(
                                color: Colors.black, fontSize: 18),
                          ),
                        ),
                        const SizedBox(height: 20),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: const Size(300, 50),
                              backgroundColor:
                                  optionpressed[1] ? Colors.white : btn2(),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          onPressed: () {
                            setState(() {
                              if (queno == 1 || queno == 3) {
                                optionpressed[1] = false;
                                marks++;
                              }
                              if (queno == 2 || queno == 4) {
                                optionpressed[1] = false;
                                optionpressed[2] = false;
                              }
                              if (queno == 5) {
                                optionpressed[1] = false;
                                optionpressed[3] = false;
                              }
                            });
                          },
                          child: Container(
                            child: Text(
                              option2[queno],
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 18),
                            ),
                          ),
                        ),
                        const SizedBox(height: 20),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: const Size(300, 50),
                              backgroundColor:
                                  optionpressed[2] ? Colors.white : btn3(),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          onPressed: () {
                            setState(() {
                              if (queno == 1 || queno == 3) {
                                optionpressed[2] = false;
                                optionpressed[1] = false;
                              }
                              if (queno == 2 || queno == 4) {
                                optionpressed[2] = false;
                                marks++;
                              }
                              if (queno == 5) {
                                optionpressed[2] = false;
                                optionpressed[3] = false;
                              }
                            });
                          },
                          child: Text(
                            option3[queno],
                            style: const TextStyle(
                                color: Colors.black, fontSize: 18),
                          ),
                        ),
                        const SizedBox(height: 20),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              minimumSize: const Size(300, 50),
                              backgroundColor:
                                  optionpressed[3] ? Colors.white : btn4(),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          onPressed: () {
                            setState(() {
                              if (queno == 1 || queno == 3) {
                                optionpressed[3] = false;
                                optionpressed[1] = false;
                              }
                              if (queno == 2 || queno == 4) {
                                optionpressed[3] = false;
                                optionpressed[2] = false;
                              }
                              if (queno == 5) {
                                optionpressed[3] = false;
                                marks++;
                              }
                            });
                          },
                          child: Text(
                            option4[queno],
                            style: const TextStyle(
                                color: Colors.black, fontSize: 18),
                          ),
                        ),
                      ],
                    )
                  : Container(
                      height: 500,
                      width: 350,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 165, 0, 194),
                              Color.fromARGB(255, 100, 1, 118),
                              Color.fromARGB(255, 33, 128, 243)
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          )),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/trophy.png",
                            height: 170,
                            width: 200,
                          ),
                          //SizedBox(height:20),
                          const Text(
                            "Congrats!",
                            style: TextStyle(
                                fontSize: 28,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),
                          ),
                          const SizedBox(height: 20),
                          Text(score[marks],
                              style: const TextStyle(
                                  fontSize: 40,
                                  fontWeight: FontWeight.w700,
                                  color: Color.fromARGB(255, 255, 197, 71))),
                          const SizedBox(height: 20),
                          const Text(
                            "Quiz Completed Successfully.",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),
                          ),
                          const SizedBox(height: 40),
                          Container(
                            alignment: Alignment.center,
                            width: 300,
                            child: Text(
                              "You attempt 5 questions and from that $marks are correct",
                              style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    )),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              queno++;
              optionpressed = [true, true, true, true, true];
              if (queno > 6) {
                queno = 1;
                marks = 0;
              }
            });
          },
          backgroundColor: const Color.fromARGB(255, 135, 15, 151),
          child: Text(text()),
        ));
  }
}
