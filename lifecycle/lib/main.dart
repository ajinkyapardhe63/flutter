import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LifeCycle(),
    );
  }
}

class LifeCycle extends StatefulWidget{

  const LifeCycle({super.key});

  State createState(){
    print("CreateState");
    return LifeCycleState();
    }
}

class LifeCycleState extends State{

  void initState(){

    super.initState();

    print("Init State");
  }

  void didChangeDependencies(){

    super.didChangeDependencies();

    print("DidChange");
  }

  Widget build(BuildContext context){

    print("Build");
    return Scaffold(
      appBar: AppBar(
        title: Text("StatefulWidget Life Cycle"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            
            ElevatedButton(onPressed: (){
              setState(() {
                
                print("Setstate");
              });
            }, child: Text("Click")),
          ],
        ),
      ),
    );

  }
}

