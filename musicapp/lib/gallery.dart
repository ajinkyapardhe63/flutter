import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:google_fonts/google_fonts.dart";

class Gallery extends StatefulWidget {
  const Gallery({super.key});
  @override
  State createState() => GalleryState();
}

class GalleryState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            alignment: Alignment.topCenter,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/gal1.png"),
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.topCenter)),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0.37, 0.43],
                colors: [
                  Color.fromARGB(0, 0, 0, 0),
                  Color.fromRGBO(24, 24, 24, 1),
                ],
              )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 220, left: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("A.L.O.N.E",
                            style: GoogleFonts.inter(
                                textStyle: const TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 33,
                                    color: Color.fromRGBO(255, 255, 255, 1)))),
                        const SizedBox(height: 10),
                        Container(
                          height: 34,
                          width: 124,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(255, 46, 0, 1),
                            borderRadius: BorderRadius.all(Radius.circular(19)),
                          ),
                          child: Center(
                            child: Text(
                              "Subscribe",
                              style: GoogleFonts.inter(
                                  fontSize: 16, fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )),
      );
  }
}
