import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:flutter/rendering.dart";
import "package:flutter/widgets.dart";
import "package:google_fonts/google_fonts.dart";
import "package:flutter_svg/flutter_svg.dart";
import "gallery.dart";

class Home1 extends StatefulWidget {
  const Home1({super.key});
  @override
  State createState() => HomeState();
}

class HomeState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      alignment: Alignment.topCenter,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/home.png"),
              fit: BoxFit.fitWidth,
              alignment: Alignment.topCenter)),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [
              0.5,
              0.75
            ],
                colors: [
              Color.fromARGB(0, 0, 0, 0),
              Color.fromRGBO(19, 19, 19, 1),
            ])),
        child: Padding(
          padding: EdgeInsets.only(top: 365),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 150,
                width: 300,
                child: Text("Dancing between The shadows    Of rhythm",
                    style: GoogleFonts.inter(
                        fontSize: 35,
                        fontWeight: FontWeight.w600,
                        color: Colors.white)),
              ),
              SizedBox(height: 30),
              GestureDetector(
                child: Container(
                  height: 45,
                  width: 250,
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(255, 46, 0, 1),
                    borderRadius: BorderRadius.all(Radius.circular(19)),
                  ),
                  child: Center(
                    child: Text(
                      "Get Started",
                      style: GoogleFonts.inter(
                          fontSize: 17, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: ((context) =>
                                              const Gallery())));
                } ,
              ),
              SizedBox(height: 15),
              GestureDetector(
                child: Container(
                  height: 45,
                  width: 250,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 47, 0, 0),
                      borderRadius: BorderRadius.all(Radius.circular(19)),
                      border: Border.fromBorderSide(
                          BorderSide(color: Color.fromRGBO(255, 46, 0, 1)))),
                  child: Center(
                    child: Text(
                      "Continue with Email",
                      style: GoogleFonts.inter(
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          color: const Color.fromRGBO(255, 46, 0, 1)),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const  EdgeInsets.only(top: 20, left: 95, right: 95),
                child: Text(
                  "by continuing you agree to terms of services and Privacy policy",
                  style: GoogleFonts.inter(
                      textStyle:const TextStyle(fontSize: 12,color:  Color.fromRGBO(203, 200, 200, 1),fontWeight: FontWeight.w500)
                      
                      ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    )
        );
  }
}
