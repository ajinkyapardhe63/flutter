import 'package:flutter/material.dart';
import 'appbar_body.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:Appbar_body(),
      debugShowCheckedModeBanner: false,
    );
  }
}

