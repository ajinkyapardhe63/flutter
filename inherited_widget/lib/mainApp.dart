import "package:flutter/material.dart";
import "package:inherited_widget/shareddata.dart";

class MainApp extends StatefulWidget {
  const MainApp({super.key});
  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  @override
  Widget build(BuildContext context) {
    Shareddata shareddataobj = Shareddata.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("InheritedWidget Demo"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Text(
            "${shareddataobj.data}",
          ),
          const SizedBox(
            height: 20,
          ),
          
        ],
      ),
    );
  }
}
