import 'package:flutter/material.dart';
import'shareddata.dart';
import'mainApp.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const Shareddata(
      data: 50, 
      child: MaterialApp(
        home: MainApp(),
      )
      );
  }
}
