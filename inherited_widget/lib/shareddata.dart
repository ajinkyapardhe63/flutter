import 'package:flutter/material.dart';

class Shareddata extends InheritedWidget {
  final int data;
  const Shareddata({super.key, required this.data, required super.child});
  static Shareddata of(BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<Shareddata>()!;

  }
  @override
  bool updateShouldNotify(Shareddata oldWidget){
    return data!=oldWidget.data;
  }
}
