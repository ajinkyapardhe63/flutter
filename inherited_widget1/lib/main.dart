import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State {
  String compName="";
  int empCount = 250;
  bool change=true;
  @override
  Widget build(BuildContext context) {
    return Company(
      compName: compName,
      empCount: empCount,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.red,
              title: const Text("Inherited Company State"),
              centerTitle: true,
            ),
            body: Column(
              
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CompanyData(),
                const SizedBox(
                  height: 60,
                ),
            
                GestureDetector(
                  onTap:(){
                    setState(() {
                      
                      compName="Nvidia";
                      empCount++;
                      
                    });
                  },
                  child:const Text("Change Company")
                ),
              ],
            )
          ),
      ),
    );
  }
}
class CompanyData extends StatelessWidget{

  const CompanyData({super.key});
  @override
  Widget build (BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
         Text(Company.of(context).compName),
        const SizedBox(
          width: 10,
        ),
        Text("${Company.of(context).empCount}"),
    ]
    );
  }
}

class Company extends InheritedWidget {
  final String compName;
  final int empCount;
  const Company({
    super.key,
    required this.compName,
    required this.empCount,
    required super.child,
  });
  static Company of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Company>()!;
  }

  @override
  bool updateShouldNotify(Company oldWidget) {
      print(empCount);
    
    return (compName) != (oldWidget.compName);
    // to change empCount u can also do : return(compName,empCount)!=(oldWidget.compName);
    //Or return(compName,empCount)!=(oldWidget.compName,oldWidget.empCount);
    //Or return compName !=oldWidget.compName || empCount!=oldWidget.empCount;
  }
}
