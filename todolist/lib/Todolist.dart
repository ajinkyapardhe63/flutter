import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class todolist extends StatefulWidget{
  const todolist({super.key});

  @override
  State createState()=>_todolistState();
}
class _todolistState extends State{
  @override
 
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Todo List"),
        centerTitle: true,
        backgroundColor: Colors.blue,
        ),
        body:
         
        Container(
          
          color:Colors.white,
          child: ListView.builder(
            itemCount: 5,
            itemBuilder: (context, index){
               Color containcolor(){
                 if (index==0){
                  return const Color.fromRGBO(250, 232, 232, 1);
                 }
                 if(index==1){
                  return const Color.fromRGBO(232, 237, 250, 1);
                 }
                 if(index==2){
                  return const Color.fromRGBO(250, 249, 232, 1);
                 }
                 if(index==3){
                  return const Color.fromRGBO(250, 232, 250, 1);
                 }
                 if(index==4){
                  return const Color.fromRGBO(250, 232, 232, 1);
                 }
                 
                 return Colors.white;

                }
              return
                
                 Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    decoration:  BoxDecoration(
                      color:containcolor(),
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                    ),
                    height: 112,
                    width: 330,
                    
                    child: Row(
                      children: [
                        Column(
                           mainAxisAlignment:MainAxisAlignment.center,
                          
                          children: [
                          
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                
                                height: 52,
                                width: 52,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white
                                ),
                                child: Image.asset("assets/gal.png"),
                              ),
                            ),

                             Text("10 July 2023",style: GoogleFonts.quicksand(fontSize:12,fontWeight: FontWeight.w500 ,color: Color.fromARGB(255, 158, 158, 149)))
                          ],
                        ),
                        Padding(
                          padding: const  EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                               const SizedBox(
                                width: 300,
                                child: Text("Lorem Ipsum is simply setting industry")),
                              const SizedBox(height: 10),
                               SizedBox(
                                width:300
                                ,child: Text("Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ",style: GoogleFonts.quicksand(fontSize:10,fontWeight: FontWeight.w500 ,color: Color.fromARGB(255, 71, 71, 71)))
                               ),
                            ],
                          ),
                        )

                      ]
                      ),
                  ),
              );
               
            }
          
          )
        ),

    );
  }
}