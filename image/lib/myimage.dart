import 'package:flutter/material.dart';

class Image2 extends StatelessWidget {
  const Image2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Hello Core2web"),
          backgroundColor: Colors.deepPurple,
          actions: [
            IconButton(icon: const Icon(Icons.search), onPressed: () {}),
            IconButton(
                icon: const Icon(Icons.favorite_rounded), onPressed: () {})
          ],
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child:
              Image.asset(
                "C:\flutter\flutter projectsimageassetsimagespanorama-city-landscape-with-empty-basketball-court-many-palms-park-sunset-summer-day-modern-grunge-3d-illustration-style-business-corporate-template_510351-2640.avif"
              ),
              ),
              const SizedBox(width: 15),
              Container(
                width: 360,
                height: 200,
                color: Colors.blue,
              ),
            ],
          ),
        ));
  }
}
