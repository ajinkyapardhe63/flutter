import "dart:async";
import "welcome.dart";
import"package:flutter/material.dart";
class SplashScreen extends StatefulWidget{
  const SplashScreen({super.key});
  @override
  State createState()=>_SplashScreen();
}
class _SplashScreen extends State{
   void initState(){
    super.initState(); 
    Timer(const Duration(seconds: 5),(){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const Welcome (), ));
    });
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:250,
                  ),
                  child: Container(
                    height:100,
                    width: 100,
                     child: Image.asset("Assets/logo.png")),
                ),
              ],
            ),
          ),
             SizedBox(height:300),
             const Text("from"),
              Row(
              mainAxisAlignment: MainAxisAlignment.center,
               children: [
                
                Image.asset("Assets/meta.png",height: 25,width: 25,),

                 const Text("META",style: TextStyle(color:Colors.green,fontSize: 15,fontWeight: FontWeight.bold),),
               ],
             )
             
        ],
      ),
    );
  }
}