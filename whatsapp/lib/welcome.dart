import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:whatsapp/login.dart";

class Welcome extends StatefulWidget {
  const Welcome({super.key});
  @override
  State createState() => _Welcome();
}

class _Welcome extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        SizedBox(
          height: 100,
        ),
        Center(
            child: Column(
          children: [
            Text(
              "Welcome to Whats'App",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 60,
            ),
            Image.asset("Assets/image 1.png"),
            SizedBox(
              height: 60,
            ),
            Container(
              height: 100,
              width: 300,
              child: Expanded(
                child: Text(
                  "Read our Privacy Policy. Tap “Agree and continue to accept the Teams of Service.",
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            GestureDetector(
              child: Container(
                height: 50,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.green, borderRadius: BorderRadius.circular(30)),
                child: Center(
                    child: Text(
                  "Agree and Continue",
                  style: TextStyle(fontSize: 16),
                )),
              ),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>const Login(), ))
            ),
             SizedBox(height:50),
             const Text("from"),
             SizedBox(height:5),
             Row(
              mainAxisAlignment: MainAxisAlignment.center,
               children: [
                
                Image.asset("Assets/meta.png",height: 25,width: 25,),

                 const Text("META",style: TextStyle(color:Colors.green,fontSize: 15,fontWeight: FontWeight.bold),),
               ],
             )
          ],
        ))
      ]),
    );
  }
}
