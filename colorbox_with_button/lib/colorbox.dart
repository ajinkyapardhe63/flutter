import 'package:flutter/material.dart';
class Colorbox extends StatelessWidget{
  const Colorbox({super.key});
  @override
  Widget build(BuildContext context){
    return  Scaffold(
      appBar: AppBar(
        title: const Text("ColorBox With Button"),
        backgroundColor: Colors.blue,
      ),
      body: SizedBox(
        height: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children:[
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.red,

                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: (){},
                  child:const Text('button1'),
                  
                ),
              ]            
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.amber,
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: (){}, 
                  child: const Text("button2")
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: const Color.fromARGB(255, 4, 0, 212),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: (){}, 
                  child: const Text("button3"))
              ],
            )
          ]
        ),
      )
    );
  }
}