import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import'drawer.dart';
import 'package:google_fonts/google_fonts.dart';

class Categorys extends StatefulWidget {
  const Categorys({super.key});

  @override
  State createState() => _CategoryState();
}

class _CategoryState extends State {
  
  List<String> img=["Assets/food.png","Assets/fuel.png","Assets/medicine.png","Assets/shopping.png"];
  List<String> txt=["Food","Fuel","medicine","shopping"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Categories",style: 
          GoogleFonts.poppins(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, // number of items in each row
            mainAxisSpacing: 20, // spacing between rows
            crossAxisSpacing: 20, // spacing between columns
          ),
          padding: EdgeInsets.all(8.0), // padding around the grid
          itemCount: 4,
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(15),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: const Offset(0, 3),
                    ),
                  ]),
              height: 145,
              width: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                Image.asset(img[index]),
                  const SizedBox(
                    height: 20,
                  ),
                   Text(
                    txt[index],
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        backgroundColor: Colors.white,
        onPressed: () {
        
        },
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                color: const Color.fromRGBO(14, 161, 125, 1),
                borderRadius: BorderRadius.circular(50),
              ),
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            const Text("  Add Category"),
          ],
        ),
      ),
        drawer: const Menu(
        ispressedtransaction: false,
        ispressedcategory: true,
        ispressedaboutus: false,
        ispressedgraph: false,
        ispressedtrash: false,)
    );
  }
}