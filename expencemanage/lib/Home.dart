import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'drawer.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State createState() => _Home();
}

class _Home extends State {
  Future<void> bottomsheet() async {
    await showModalBottomSheet(
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                top: 30,
                left: 15,
                right: 15,
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: Form(
                // key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: main,
                  children: [
                    Text(
                      "Date",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      // controller: descriptionController,
                      // maxLines: 1,
                      decoration: InputDecoration(
                        // label: const Text("11-06-2022"),
                        hintText: "DD/MM/YYYY",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(107, 112, 92, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Amount",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      // controller: descriptionController,
                      // maxLines: 1,
                      decoration: InputDecoration(
                        // label: const Text("900"),
                        hintText: "900",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(107, 112, 92, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Category",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      // controller: descriptionController,
                      // maxLines: 1,
                      decoration: InputDecoration(
                        // label: const Text("900"),
                        hintText: "Shopping",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(107, 112, 92, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Description",
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                              fontSize: 13, fontWeight: FontWeight.w400)),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    TextFormField(
                      // controller: descriptionController,
                      // maxLines: 1,
                      decoration: InputDecoration(
                        // label: const Text("900"),
                        hintText: "Lorem Ipsum is simply dummy text of the ",
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(107, 112, 92, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(4, 161, 125, 1),
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        height: 49,
                        child: Center(
                          child: Text(
                            "ADD",
                            style: GoogleFonts.poppins(
                              textStyle: const TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "June 2022",
          style: GoogleFonts.poppins(
              textStyle:
                  const TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
        ),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.search))],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: ListView.builder(
          itemCount: 4,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(top: 15, left: 12, right: 19),
              child: Container(
                height: 63,
                width: 361.5,
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color.fromRGBO(206, 206, 206, 1),
                    ),
                  ),
                ),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 43,
                          width: 43,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(0, 174, 91, 0.7),
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 17,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Medicine",
                                style: GoogleFonts.poppins(
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 15,
                                    color: Color.fromRGBO(0, 0, 0, 1),
                                  ),
                                ),
                              ),
                              Text(
                                "Lorem Ipsum is simply dummy text of the ",
                                style: GoogleFonts.poppins(
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 10,
                                    color: Color.fromRGBO(0, 0, 0, 0.8),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            const Icon(
                              Icons.remove_circle_rounded,
                              size: 15,
                              color: Color.fromRGBO(246, 113, 49, 1),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              "500",
                              style: GoogleFonts.poppins(
                                textStyle: const TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 15,
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "3 June | 11:50 AM",
                          style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 10,
                              color: Color.fromRGBO(0, 0, 0, 0.6),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
        backgroundColor: Colors.white,
        onPressed: () {
          bottomsheet();
        },
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                color: const Color.fromRGBO(14, 161, 125, 1),
                borderRadius: BorderRadius.circular(50),
              ),
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
            const Text("  Add Transaction"),
          ],
        ),
      ),
      drawer: const Menu(
        ispressedtransaction: true,
        ispressedcategory: false,
        ispressedaboutus: false,
        ispressedgraph: false,
        ispressedtrash: false, 
      ),
    );
  }
}
