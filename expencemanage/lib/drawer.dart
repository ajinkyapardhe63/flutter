import 'package:expencemanage/Home.dart';
import 'package:expencemanage/aboutus.dart';
import 'package:expencemanage/trash.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'catogery.dart';
import'graph.dart';

class Menu extends StatefulWidget {
  final bool ispressedtransaction;
  final bool ispressedcategory;
  final bool ispressedgraph;
  final bool ispressedtrash;
  final bool ispressedaboutus;
  const Menu({
    super.key,
    required this.ispressedtransaction,
    required this.ispressedcategory,
    required this.ispressedgraph,
    required this.ispressedtrash,
    required this.ispressedaboutus,
  });
  @override
  State<Menu> createState() => _Menu();
}

class _Menu extends State<Menu> {
  // bool ispressedcategory = false;
  // bool ispressedgraph = false;
  // bool ispressedtrash = false;
  // bool ispressedaboutus = false;
  List<Color> clr = [
    const Color.fromRGBO(14, 161, 125, 0.15),
    const Color.fromRGBO(14, 161, 125, 1),
    const Color.fromARGB(0, 255, 255, 255),
    Colors.black
  ];
  List<Color> clr1() {
    if (widget.ispressedtransaction == true) {
      return [
         clr[0],
        clr[1]
      ];
    }
    return [clr[2],clr[3]];
  }

  List<Color> clr2() {
    if (widget.ispressedgraph == true) {
      return [
        clr[0],
        clr[1]
      ];
    }
    return [clr[2],clr[3]];
  }

  List<Color> clr3() {
    if (widget.ispressedcategory == true) {
      return [
        clr[0],
        clr[1]
      ];
    }
    return [clr[2],clr[3]];
  }

  List<Color> clr4() {
    if (widget.ispressedtrash == true) {
      return [
        clr[0],
        clr[1]
      ];
    }
    return [clr[2],clr[3]];
  }

  List<Color> clr5() {
    if (widget.ispressedaboutus == true) {
      return [
        clr[0],
        clr[1]
      ];
    }
    return [clr[2],clr[3]];
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30, left: 30, bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Expense Manager",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600, fontSize: 18)),
                Text("Save all your Transactions",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: const Color.fromRGBO(0, 0, 0, 0.5))),
              ],
            ),
          ),
          GestureDetector(
            child: Container(
              height: 60,
              width: 250,
              decoration: BoxDecoration(
                  color: clr1()[0],
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(40),
                      bottomRight: Radius.circular(40))),
              child: Padding(
                padding: EdgeInsets.only(left: 30),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "Assets/svgimg/transaction.svg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Transaction",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w400,
                            color: clr1()[1],
                            fontSize: 18)),
                  ],
                ),
              ),
            ),
            onTap: () {
              setState(() {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ));
              });
            },
          ),
          SizedBox(height: 10),
          GestureDetector(
              child: Container(
                height: 60,
                width: 250,
                decoration: BoxDecoration(
                    color: clr2()[0],
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Padding(
                  padding: EdgeInsets.only(left: 30),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        "Assets/svgimg/graph.svg",
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text("Graph",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: clr2()[1])),
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Graphs(),
                    ));
              });
              }),
          SizedBox(height: 10),
          GestureDetector(
              child: Container(
                height: 60,
                width: 250,
                decoration: BoxDecoration(
                    color: clr3()[0],
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        "Assets/svgimg/category.svg",
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text("Category",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: clr3()[1])),
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Categorys(),
                      ));
                });
              }),
          SizedBox(height: 10),
          GestureDetector(
              child: Container(
                height: 60,
                width: 250,
                decoration: BoxDecoration(
                    color: clr4()[0],
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Padding(
                  padding: EdgeInsets.only(left: 30),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        "Assets/svgimg/trash.svg",
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text("Trash",
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            color: clr4()[1],
                          )),
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Trash(),
                    ));
              });
              }),
          SizedBox(height: 10),
          GestureDetector(
              child: Container(
                height: 60,
                width: 250,
                decoration: BoxDecoration(
                    color: clr5()[0],
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(40),
                        bottomRight: Radius.circular(40))),
                child: Padding(
                  padding: EdgeInsets.only(left: 30),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        "Assets/svgimg/person.svg",
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text("About Us",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                              color: clr5()[1])),
                    ],
                  ),
                ),
              ),
              onTap: () {
                setState(() {
                  
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AboutUs(),
                    ));
              });
              }),
        ],
      ),
    );
  }
}
