import 'dart:async';
import'package:flutter/material.dart';
import'signup.dart';
import'package:google_fonts/google_fonts.dart';
class SplashScreen extends StatefulWidget{
  const SplashScreen({super.key});
  @override
  State createState()=>_SplashScreen();
}
class _SplashScreen extends State{
  @override
  void initState(){
    super.initState(); 
    Timer(const Duration(seconds: 4),(){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const Signup(), ));
    });
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(

      body: Column(
        
        mainAxisAlignment: MainAxisAlignment.center,
          children:[ Center(
            child: Padding(
              padding: const EdgeInsets.only(top:310,bottom:290),
              child: Container(
                    height: 144,
                    width: 144,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(234, 238, 235, 1)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:22,right:22,top:10,bottom:22),
                      child: Image.asset("Assets/ss.png"),
                    )
              ),
            ),
            
          ),
          
           Text("Expence Manager",style: GoogleFonts.poppins(fontWeight:FontWeight.w600,fontSize:16,color:Colors.black),),
          ]    
      ),
    );
  }
}