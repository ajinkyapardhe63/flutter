import 'package:expencemanage/signup.dart';
import 'package:flutter/material.dart';
import 'Home.dart';

import 'package:google_fonts/google_fonts.dart';

class Signin extends StatefulWidget {
  const Signin({super.key});
  @override
  State createState() => _Signin();
}

class _Signin extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 89,
            ),
            Center(child: Image.asset("Assets/ss.png")),
            const SizedBox(
              height: 60,
            ),
            Center(
              child: Container(
                height: 265,
                width: 320,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Login to your Account",
                        style: GoogleFonts.poppins(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Container(
                        height: 49,
                        width: 320,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.15),
                                  offset: Offset(0, 3),
                                  blurRadius: 10)
                            ]),
                        child: TextField(
                        
                          decoration: InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            enabledBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                borderSide: BorderSide(color: Colors.white)),
                                focusedBorder:const OutlineInputBorder(
                                  borderSide:BorderSide(color: Colors.white),
                                  borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                ),
                            hintText: "Username",
                            hintStyle: GoogleFonts.poppins(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: const Color.fromRGBO(0, 0, 0, 0.4)),
                          ),
                        ),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        height: 49,
                        width: 320,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.15),
                                  offset: Offset(0, 3),
                                  blurRadius: 10)
                            ]),
                        child: TextField(
                          decoration: InputDecoration(
                            fillColor: Colors.white,
                            filled: true,
                            enabledBorder: const OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                borderSide: BorderSide(color: Colors.white)),
                                focusedBorder:const OutlineInputBorder(
                                  borderSide:BorderSide(color: Colors.white),
                                  borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                ),
                            hintText: "Password",
                            hintStyle: GoogleFonts.poppins(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: const Color.fromRGBO(0, 0, 0, 0.4)),
                          ),
                        ),
                      ),
                      const SizedBox(height: 35),
                      GestureDetector(
                        child: Container(
                            height: 49,
                            width: 320,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: Color.fromRGBO(14, 161, 125, 1)),
                            child: Center(
                                child: Text("Sign In",
                                    style: GoogleFonts.poppins(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white)))),
                        onTap: () => Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const Home(),
                            )),
                      )
                    ]),
              ),
            ),
            const SizedBox(
              height: 275,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Don't have an account ",
                  style: GoogleFonts.poppins(
                      color: const Color.fromRGBO(0, 0, 0, 0.6),
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
                GestureDetector(
                  child: Text(
                    "Sign up",
                    style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(14, 161, 125, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400),
                  ),
                  onTap: () => Navigator.pushReplacement(context,MaterialPageRoute(builder: (context)=>Signup()))
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
