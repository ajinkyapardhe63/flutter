import 'package:flutter/material.dart';
import'drawer.dart';
import 'package:google_fonts/google_fonts.dart';

class Trash extends StatefulWidget {
  const Trash({super.key});

  @override
  State createState() => _Trash();
}

class _Trash extends State {
  @override
  Widget build(BuildContext context){
    return  Scaffold(
      appBar: AppBar(
        title: Text("Trash",style: GoogleFonts.poppins(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),),
        
      ),
      drawer:  const Menu(ispressedaboutus: false,ispressedcategory: false,ispressedgraph: false,ispressedtransaction: false,ispressedtrash: true,)
    );
  }
}
