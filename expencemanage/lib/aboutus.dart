import 'package:flutter/material.dart';
import'drawer.dart';
import 'package:google_fonts/google_fonts.dart';
class AboutUs extends StatefulWidget {
  const AboutUs({super.key});

  @override
  State createState() => _AboutUs();
}

class _AboutUs extends State {
  @override
  Widget build(BuildContext context){
    return  Scaffold(
      appBar: AppBar(
        title: Text("About Us",style: GoogleFonts.poppins(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          )),
      
      ),
      drawer:  const Menu(ispressedaboutus: true,ispressedcategory: false,ispressedgraph: false,ispressedtransaction: false,ispressedtrash: false,)
    );
  }
}
