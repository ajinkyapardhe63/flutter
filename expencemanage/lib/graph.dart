import 'dart:ffi';

import 'package:flutter/material.dart';
import 'drawer.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Graphs extends StatefulWidget {
  const Graphs({super.key});

  @override
  State createState() => _Graphs();
}

class _Graphs extends State {
  List<Color> clr = [
    const Color.fromRGBO(214, 3, 3, 0.7),
    const Color.fromRGBO(0, 148, 255, 0.7),
    const Color.fromRGBO(0, 174, 91, 0.7),
    const Color.fromRGBO(100, 62, 255, 0.7),
    const Color.fromRGBO(241, 38, 197, 0.7),
  ];
  Map<String, double> data = {
    "Food": 650,
    "Fuel": 600,
    "Medicine": 500,
    "Entertainment": 475,
    "Shopping": 325
  };
  List<String> img = [
    "Assets/food.png",
    "Assets/fuel.png",
    "Assets/medicine.png",
    "Assets/entertainment.png",
    "Assets/shopping.png",
  ];
  
  @override
  Widget build(BuildContext context) {
    double sum = data.values.reduce((value, element) => value + element);
    return Scaffold(
        appBar: AppBar(
          title: Text("Graph",
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              )),
        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, bottom: 50),
          child: Center(
            child: SizedBox(
              height: 660,
              width: 300,
              child: Column(
                children: [
                  const SizedBox(height: 30),
                  Center(
                    child: PieChart(
                      colorList: [clr[0], clr[1], clr[2], clr[3], clr[4]],
                      dataMap: data,
                      animationDuration: const Duration(milliseconds: 2000),
                      chartType: ChartType.ring,
                      chartRadius: 200,
                      ringStrokeWidth: 30,
                      centerTextStyle:GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w600, 
                                fontSize: 10)
                                ) ,
                      //centerText: "Total",
                      centerWidget: Column(
                        children: [
                          SizedBox(height: 40,),
                          Text("Total",style:GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400, 
                                fontSize: 10)
                                )  ,),
                          Text("₹ ${sum.toString()}0",style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w600, 
                                fontSize: 13)
                                ) ,),
                        ],
                      ),
                      chartValuesOptions: const ChartValuesOptions(
                        showChartValues: false,
                        showChartValueBackground: false,
                        chartValueBackgroundColor: Colors.white,
                        showChartValuesInPercentage: false,
                      ),
                      legendOptions: LegendOptions(
                        legendTextStyle: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 12)),
                        legendPosition: LegendPosition.right,
                        legendShape: BoxShape.circle,
                        showLegends: true,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Divider(),
                  const SizedBox(
                    height: 20,
                  ),
                  ListView.builder(
                      
                      itemCount: 5,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            
                            Row(
                              children: [
                                const SizedBox(
                                  width: 20,
                                ),
                                SizedBox(
                                  height: 40,
                                  width: 40,
                                  child: Image.asset(img[index]),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  data.keys.elementAt(index),
                                  style: GoogleFonts.poppins(
                                      textStyle: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15)),
                                ),
                                const Spacer(),
                                
                                Text(
                                    "₹ ${data.values.elementAt(index).toString()}",
                                    style: GoogleFonts.poppins(
                                        textStyle: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15)))
                              ],
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                          ],
                        );
                      }),
                  const Divider(),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 30,
                      ),
                      Text(
                        "Total",
                        style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w400, 
                                fontSize: 16)
                                ),
                      ),
                      const Spacer(),
                      Text("₹ ${sum.toString()}0",style: GoogleFonts.poppins(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w600, 
                                fontSize: 15)
                                ),),
                      const SizedBox(width: 30,)
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        drawer: const Menu(
          ispressedaboutus: false,
          ispressedcategory: false,
          ispressedgraph: true,
          ispressedtransaction: false,
          ispressedtrash: false,
        ));
  }
}
