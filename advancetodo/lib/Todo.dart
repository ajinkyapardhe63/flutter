// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';

dynamic database;

class MyToDoApp extends StatefulWidget {
  const MyToDoApp({super.key});

  @override
  State<MyToDoApp> createState() => _MyToDoApp();
}

class _MyToDoApp extends State<MyToDoApp> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController datePickerController = TextEditingController();

  bool titleValidate = false;
  bool desValidate = false;
  bool dateValidate = false;

  bool _onclick = true;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // ignore: non_constant_identifier_names
  List<ToDoModelClass> card_list = [
    ToDoModelClass(
      title: "Buy grocerry from DMart ",
      description: " kirana an all things ",
      date: "Mar 5, 2024",
    ),
    ToDoModelClass(
      title: "Meeting Schendule ",
      description: " Project Related",
      date: "Mar 6, 2024",
    ),
    ToDoModelClass(
      title: "virtual Schendule ",
      description: " Project Related",
      date: "Mar 6, 2024",
    ),
    ToDoModelClass(
      title: "trip planned ",
      description: " Goa Related",
      date: "Mar 6, 2024",
    ),
  ];

  List color_list = const [
    Color.fromRGBO(255, 249, 244, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1),
  ];

  Future addBottomsheet() {
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Form(
              key: _formKey,
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(" Create Task ",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 22,
                              color: Colors.black,
                            )),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      height: 60,
                      width: double.infinity,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 5),
                      child: TextFormField(
                        controller: titleController,
                        // key: userNameKey,
                        decoration: InputDecoration(
                          hintText: " Enter Title",
                          label: const Text(" Title"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.note_add_outlined,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter title";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                      child: TextFormField(
                        controller: descriptionController,
                        // key: passwordKey,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          label: const Text("Enter Description"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.notes_sharp,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter Description";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  String enteredTitle =
                                      titleController.text.trim();
                                  String enteredDescription =
                                      descriptionController.text.trim();
                                  String enteredDate =
                                      datePickerController.text.trim();

                                  enteredTitle.isEmpty
                                      ? titleValidate = true
                                      : titleValidate = false;
                                  enteredDescription.isEmpty
                                      ? desValidate = true
                                      : desValidate = false;
                                  enteredDate.isEmpty
                                      ? dateValidate = true
                                      : dateValidate = false;

                                  if (dateValidate == false &&
                                      titleValidate == false &&
                                      desValidate == false) {
                                    card_list.add(ToDoModelClass(
                                      title: titleController.text,
                                      description: descriptionController.text,
                                      date: datePickerController.text,
                                    ));

                                    titleController.text = "";
                                    descriptionController.text = '';
                                    datePickerController.text = '';

                                    titleValidate = false;
                                    desValidate = false;
                                    dateValidate = false;

                                    Navigator.pop(context);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text("Task added Successfully"),
                                      ),
                                    );
                                  } else {
                                    Navigator.pop(context);
                                    titleController.clear();
                                    descriptionController.clear();
                                    datePickerController.clear();

                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text("Task Failed"),
                                      ),
                                    );
                                  }
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              child: const Text(
                                " Add ",
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

//==================================================================================================
//==================================================================================================
  Future editBottomsheet(ToDoModelClass obj, int index) {
    titleController.text = obj.title;
    descriptionController.text = obj.description;
    datePickerController.text = obj.date;

    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Form(
              key: _formKey,
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(" Create Task ",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 22,
                              color: Colors.black,
                            )),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      height: 60,
                      width: double.infinity,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 5),
                      child: TextFormField(
                        controller: titleController,
                        // key: userNameKey,
                        decoration: InputDecoration(
                          hintText: " Enter Title",
                          label: const Text(" Title"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.note_add_outlined,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter title";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                      child: TextFormField(
                        controller: descriptionController,
                        // key: passwordKey,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          label: const Text("Enter Description"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.notes_sharp,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter Description";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  String enteredTitle =
                                      titleController.text.trim();
                                  String enteredDescription =
                                      descriptionController.text.trim();
                                  String enteredDate =
                                      datePickerController.text.trim();

                                  enteredTitle.isEmpty
                                      ? titleValidate = true
                                      : titleValidate = false;
                                  enteredDescription.isEmpty
                                      ? desValidate = true
                                      : desValidate = false;
                                  enteredDate.isEmpty
                                      ? dateValidate = true
                                      : dateValidate = false;

                                  if (dateValidate == false &&
                                      titleValidate == false &&
                                      desValidate == false) {
                                    card_list[index] = ToDoModelClass(
                                      title: titleController.text,
                                      description: descriptionController.text,
                                      date: datePickerController.text,
                                    );

                                    Navigator.pop(context);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text("Task Updated Successfully"),
                                      ),
                                    );
                                  } else {
                                    Navigator.pop(context);
                                    titleController.clear();
                                    descriptionController.clear();
                                    datePickerController.clear();

                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text("Task Not Updated"),
                                      ),
                                    );
                                  }
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              child: const Text(
                                " Update ",
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget addListViewBuilder() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: card_list.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: Container(
            width: double.infinity,
            height: 110,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Color.fromARGB(255, 247, 247, 247),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  offset: Offset(0, 8),
                  blurRadius: 5,
                  spreadRadius: 1,
                ),
              ],
            ),
            child: Slidable(
              endActionPane: ActionPane(
                motion: const ScrollMotion(),
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 28.0),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            editBottomsheet(card_list[index], index);
                          },
                          icon: const Icon(Icons.edit),
                          color: const Color.fromRGBO(89, 57, 241, 1),
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        IconButton(
                          onPressed: () {
                            showAlertDialog(BuildContext context) {
                              Widget cancelButton = TextButton(
                                child: const Text("Cancel"),
                                onPressed: () {
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                },
                              );

                              Widget deleteButton = TextButton(
                                child: const Text("Delete"),
                                onPressed: () {
                                  setState(() {
                                    card_list.removeAt(index);
                                    Navigator.of(context).pop();
                                  });
                                },
                              );

                              AlertDialog alert = AlertDialog(
                                title: const Text("Alert Dialog"),
                                content: const Text("Are you sure to delete ?"),
                                actions: [
                                  cancelButton,
                                  deleteButton,
                                ],
                              );
                              // show the dialog
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                },
                              );
                            }

                            setState(() {
                              showAlertDialog(context);
                            });
                          },
                          icon: const Icon(Icons.delete),
                          color: const Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          height: 52,
                          width: 52,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border.all(width: 0.3),
                            shape: BoxShape.circle,
                            color: color_list[index % color_list.length],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 0.0,
                            ),
                            child: Text(
                              "${index + 1}",
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(55, 58, 91, 1),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: 10),
                  SizedBox(
                    width: 250,
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Text(
                              card_list[index].title,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                            )),
                          ],
                        ),
                      ),
                      const SizedBox(height: 2),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              card_list[index].description,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              )),
                            )),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                        child: Spacer(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          children: [
                            Text(
                              card_list[index].date,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                            ),
                            const SizedBox(
                              width: 13,
                            ),
                          ],
                        ),
                      ),
                    ]),
                  ),
                  const Spacer(),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          // child:  Icon(
                          //   Icons.check_circle_outline,
                          //   color: Colors.green),
                          child: IconButton(
                              iconSize: 30,
                              onPressed: () {
                                setState(() {
                                  _onclick = !_onclick;
                                });
                              },
                              icon: _onclick
                                  ? const Icon(
                                      Icons.check_circle_outline,
                                    )
                                  : const Icon(
                                      Icons.check_circle,
                                      color: Colors.green,
                                    )),
                        ),
                      ]),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 40),
          SizedBox(
            height: 30,
            width: 250,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Good Morning , ",
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                )),
              ),
            ),
          ),
          SizedBox(
            height: 35,
            width: 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 22.0),
              child: Text(
                "Ajinkya",
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                )),
              ),
            ),
          ),
          const SizedBox(height: 20),
          Expanded(
            child: Container(
              height: 600,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(217, 217, 217, 1),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  topLeft: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "CREATE TO DO LIST ",
                      style: GoogleFonts.quicksand(
                          textStyle: const TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Expanded(
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40),
                          topLeft: Radius.circular(40),
                        ),
                      ),
                      child: addListViewBuilder(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addBottomsheet,
        tooltip: "Add task",
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        child: const Icon(
          Icons.add,
          size: 40,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}

class ToDoModelClass {
  final String title;
  final String description;
  final String date;

  ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}
