import 'package:flutter/material.dart';
import 'Todo.dart';
void main() {
  runApp(const MainApp());
  TimeOfDay now = TimeOfDay.now();
  print(now.hour);
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyToDoApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}
