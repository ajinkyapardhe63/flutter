
import'package:sqflite/sqflite.dart';
import'package:flutter/widgets.dart';
import'package:path/path.dart';


dynamic database;
class Todo{
  final int id;
  final String title;
  final String  description;
  final String date;

  Todo({
    required this.id,
    required this.title,
    required this.description,
    required this.date,
  });
  Map<String,dynamic>PlayerMap(){
    return{
      'id':id,
      'title':title,
      'description':description,
      'date':date,
    };
  }
  @override
  String toString(){
    return '{id:$id,title:$title,description:$description,date:$date,}';
  }

}
Future insertPlayerData(Todo obj)async{
  final localDB=await database;
  await localDB.insert(
    'Player',
    obj.PlayerMap(),
    conflictAlgorithm:ConflictAlgorithm.replace,
  );
}
Future<List<Todo>>getPlayerData()async{
  final localDB=await database;
  List<Map<String,dynamic>> listPlayers=await localDB.query("Player");
  return List.generate(listPlayers.length, (i) {
    return Todo(
      id: listPlayers[i]['id'],
      title:listPlayers[i]['title'],
      description: listPlayers[i]['description'],
      date: listPlayers[i]["date"],
    );
  });
}
void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  database=openDatabase(
    join(await getDatabasesPath(),"TodoDB.db"),
    version: 1,
    onCreate: (db,version)async{
      await db.execute('''CREATE TABLE Todo(
        id  INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        description TEXT,
        date TEXT)''');
    },
  );
  Todo obj1=Todo(id:1, title: "Shoping", description: "Done", date: "10/7/2023");
  insertPlayerData(obj1);
  Todo obj2=Todo(id:1, title: "Shoping", description: "Done", date: "10/7/2023");
  insertPlayerData(obj2);
   Todo obj3=Todo(id:1, title: "Shoping", description: "Done", date: "10/7/2023");
  insertPlayerData(obj3);

  print(await getPlayerData());


}