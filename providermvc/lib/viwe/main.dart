import 'package:flutter/material.dart';
import 'package:providermvc/viwe/productdetails.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ProductDetails(),
      debugShowCheckedModeBanner: false,
    );
  }
}
