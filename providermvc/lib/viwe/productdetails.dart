import 'package:flutter/material.dart';

class ProductDetails extends StatefulWidget{
  const ProductDetails({super.key});
  @override
  State createState()=> _ProductDetails();
}
class _ProductDetails extends State{
  TextEditingController url=TextEditingController();
  TextEditingController pname=TextEditingController();
  TextEditingController pprice=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Product Details"),
        centerTitle: true,
      ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: url,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Image URL",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                   Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: pname,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Product Name",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                   Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: pprice,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Product Name",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}