import 'package:flutter/material.dart';
class MyQuizApp extends StatefulWidget {
  const MyQuizApp({super.key});

  State createState() => QuizState();
}
class SingleQuestion{
  final String? question;
  final List<String>? options;
  final int? answerIndex;
  const SingleQuestion({this.question,this.options,this.answerIndex});

}

class QuizState extends State {
  int Qcount = 1;
  int score = 0;
  bool questionS = true;
  int QuestionIndex = 0;
  int ChoosenIndex = -1;
  

  MaterialStateProperty<Color?> colorChange(int buttonIndex) {
    if (ChoosenIndex != -1) {
      if (buttonIndex == qna[QuestionIndex].answerIndex) {
        
        return MaterialStatePropertyAll(Colors.green);
        
      } else if (ChoosenIndex == buttonIndex) {
        return MaterialStatePropertyAll(Colors.red);
      } else {
        return MaterialStatePropertyAll(null);
      }
    } else {
      return MaterialStatePropertyAll(null);
    }
  }

  List qna = [
    const SingleQuestion (
    
      question: "Who is the founder of Google?",
      options: ["lary Page", "Bill Gates", "jeff Bezoz", "Steve Jobs"],
      answerIndex: 0,
    
    ),
    const SingleQuestion(
      question: "Who is the founder of Microsoft?",
      options: ["lary Page", "Bill Gates", "jeff Bezoz", "Steve Jobs"],
      answerIndex: 1,
    ),
    const SingleQuestion(
      question: "Who is the founder of Amazon?",
      options: ["lary Page", "Bill Gates", "jeff Bezoz", "Steve Jobs"],
      answerIndex: 2,
    ),

    const SingleQuestion(
      question: "Who is the founder of Apple?",
      options: ["lary Page", "Bill Gates", "jeff Bezoz", "Steve Jobs"],
      answerIndex: 3,
    ),
    const SingleQuestion(
      question: "Who is the founder of SpaceX?",
      options: ["lary Page", "Bill Gates", "Elon Musk", "Steve Jobs"],
      answerIndex: 2,
    ),
  ];

  Scaffold isQuestionScreen() {
    if (questionS == true) {
      return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration:const BoxDecoration(
              gradient:LinearGradient(colors: [Colors.blue,Colors.purple],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              ),
               
            ),
            
          ),
          centerTitle: true,
          title: const Text("Tech Quiz"),
        ),
        body: Container(

           
            color:Color.fromARGB(255, 244, 200, 255),   
          child: Column(children: [
            const SizedBox(height: 20),
            
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Question :",
                  style: TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                ),
                const SizedBox(
                  width: 5,
                ),
                Text(
                  "${QuestionIndex + 1}/${qna.length}",
                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w900),
                ),
              ],
            ),

            const SizedBox(height: 25),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  qna[QuestionIndex].question,
                  style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),

            SizedBox(
              height: 40,
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  if (ChoosenIndex == -1) {
                    setState(() {
                      ChoosenIndex = 0;
                    });
                  }
                },
                style: ButtonStyle(backgroundColor: colorChange(0)),
                child: Text(
                  "A.${qna[QuestionIndex].options[0]}",
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.normal),
                ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),

            SizedBox(
              height: 40,
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  if (ChoosenIndex == -1) {
                    setState(() {
                      ChoosenIndex = 1;
                    });
                  }
                },
                style: ButtonStyle(
                  backgroundColor: colorChange(1),
                ),
                child: Text(
                  "B.${qna[QuestionIndex].options[1]}",
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.normal),
                ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),

            SizedBox(
              height: 40,
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  if (ChoosenIndex == -1) {
                    setState(() {
                      ChoosenIndex = 2;
                    });
                  }
                },
                style: ButtonStyle(
                  backgroundColor: colorChange(2),
                ),
                child: Text(
                  "C.${qna[QuestionIndex].options[2]}",
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.normal),
                ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 40,
              width: 200,
              child: ElevatedButton(
                onPressed: () {
                  if (ChoosenIndex == -1) {
                    setState(() {
                      ChoosenIndex = 3;
                    });
                  }
                },
                style: ButtonStyle(
                  backgroundColor: colorChange(3),
                ),
                child: Text(
                  "D.${qna[QuestionIndex].options[3]}",
                  style: const TextStyle(
                      fontSize: 18, fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (ChoosenIndex == -1) {
              return;
            }
            if (ChoosenIndex == qna[QuestionIndex].answerIndex) {
              score++;
            }
            ChoosenIndex = -1;
            QuestionIndex++;
            if (QuestionIndex == qna.length) {
              questionS = false;
            }
            setState(() {});
          },
          child: const Icon(Icons.keyboard_arrow_right),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration:const BoxDecoration(
              gradient:LinearGradient(colors: [Colors.blue,Colors.purple],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              ),
               
            ),
            
          ),
          centerTitle: true,
          title: const Text("Score"),
        ),
        body: Column(
          
          children: [
            const SizedBox(
              height: 15,
            ),
            Image.network(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3zlXC3yOOD9du2iA86MCrukZwuVSrLO6yqg&usqp=CAU",
              height: 300,
              width: 500,
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              "Congratulations",
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "You Scored: $score",
              style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
            ),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
              height: 50,
              width: 100,
              child: ElevatedButton(
                onPressed: () {
                  ChoosenIndex = -1;
                  questionS = true;
                  QuestionIndex = 0;
                  setState(() {});
                },
                child: const Text(
                  "Reset",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
