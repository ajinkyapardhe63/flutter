import 'dart:developer';

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("In main App");
    return  MaterialApp( // Add 'log("in materil app")' in the Material App 'build method '
      home: Scaffold( // Add 'log("in Scaffold app")' in the Scaffold 'build method'
        appBar:AppBar(// Add 'log("in  appbar Build")' in the AppBar 'build method' 
          backgroundColor: Colors.red,
          title:const Text("Demo")//Add 'log("in Text Build")' in the  Text'build method' 
        ),
        body: const Center(
          child: Text('Hello World!'),
        ),
        floatingActionButton: FloatingActionButton( //Add 'log("in FloatingActionButton Build")' in the  FloatingActionButton 'build method' 
        onPressed: () {
          
        },),
      ),
    );
  }
}
