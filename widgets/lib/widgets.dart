import 'package:flutter/material.dart';

class widgets extends StatefulWidget {
  const widgets({super.key});
  @override
  State createState() => _widgetstate();
}

class _widgetstate extends State {
  bool change=true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            "Spotify",
            style: TextStyle(color: Colors.green, fontSize: 22),
          ),
          backgroundColor: Colors.black,
        ),
        body: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.black, Colors.black],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight),
            ),
            child:SingleChildScrollView(
              scrollDirection: Axis.vertical,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const SizedBox(width:15,),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 57, 57, 57),shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
                        onPressed: () {},
                        child: const Text("All", style: TextStyle(fontSize: 18))),
                    const SizedBox(
                      width: 10,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor:change==true?
                                const Color.fromARGB(255, 57, 57, 57):const Color.fromARGB(255, 25, 96, 27),shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
                        onPressed: () {
                          setState(() {
                            change=!change;
                          });
                        },
                        child: const Text("Music", style: TextStyle(fontSize: 18))),
                    const SizedBox(
                      width: 10,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(255, 57, 57, 57),shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
                        onPressed: () {},
                        child: const Text("Podcast", style: TextStyle(fontSize: 18))),
                  ],
                ),
                const SizedBox(height: 30),
                const Text(
                  "  Made For Ajinkya",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 20),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child:Row(

                    children: [
                      const SizedBox(width:15,),
                      Column(
                        children: [
                          Image.asset("assets/ar.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color:Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      const SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/poorvesh.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("VIBIE,poorvesh Dave,Slient Ocean and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      const SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/pritam.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      )
                    ],
                  )
                ),
                const SizedBox(height: 30),
                const Text(
                  "  International!",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 20),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child:Row(
                    children: [
                      SizedBox(width:15,),
                      Column(
                        children: [
                          Image.asset("assets/lil.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color:Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/friday.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("VIBIE,poorvesh Dave,Slient Ocean and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/hot.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      )
                    ],
                  )
                ),
                const SizedBox(height: 30),
                const Text(
                  "Popular radio",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 20),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child:Row(
                    children: [
                      Column(
                        children: [
                          Image.asset("assets/lil.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color:Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/friday.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("VIBIE,poorvesh Dave,Slient Ocean and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      ),
                      SizedBox(width: 20,),
                      Column(
                        children: [
                          Image.asset("assets/hot.jpg",height: 180,width: 180,),
                          const SizedBox(height: 10,),
                          Container(
                            width:180,child: const Text("A.R.Rahaman,Mitraz,pritam and more",style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 174, 174, 174)),textAlign: TextAlign.center,)),

                        ],
                        
                      )
                    ],
                  )
                ),
                //  Align(
                // alignment: Alignment.bottomCenter,
                // child: Theme(
                //     data: Theme.of(context)
                //         .copyWith(canvasColor: Colors.transparent),
                //     child: BottomNavigationBar(
                //       currentIndex: 0,
                //       items: [
                //         BottomNavigationBarItem(
                //             icon: Icon(Icons.home), label: 'Home'),
                //         BottomNavigationBarItem(
                //             icon: Icon(Icons.home),  label:'Home'),
                //         BottomNavigationBarItem(
                //             icon: Icon(Icons.home), label: 'Home')
                //       ],
                //     ))),
              ],
            )
            )
            ),
            
      //       bottomNavigationBar: BottomNavigationBar(
      //         //backgroundColor: Colors.black,
      //   items: const [
      //     BottomNavigationBarItem(
      //       backgroundColor: Color.fromARGB(0, 255, 255, 255),
      //       icon: Icon(
      //         Icons.home,
      //         color: Color.fromARGB(255, 255, 255, 255),
      //         size: 30,
      //       ),
      //       label: '',
      //     ),
      //     BottomNavigationBarItem(
            
      //         icon: Icon(
      //           Icons.search,
      //          color: Color.fromARGB(255, 255, 255, 255),
      //           size: 40,
      //         ),
      //         label: 'Search',
      //         ),
      //     BottomNavigationBarItem(
      //       icon: Icon(
      //         Icons.library_music_outlined,
      //         color: Color.fromARGB(255, 255, 255, 255),
      //         size: 40,
      //       ),
      //       label: 'Favorites',
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(
      //         Icons.favorite,
      //         color: Color.fromARGB(255, 255, 255, 255),
      //         size: 35,
      //       ),
      //       label: 'Cart',
      //     ),
          
      //   ],
      // ),
            
            );
  }
}
