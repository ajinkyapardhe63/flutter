import 'package:flutter/material.dart';

class Containerdecor extends StatelessWidget {
  const Containerdecor({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Container Decorator",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white)),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.brown, Colors.black],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight)),
          ),
        ),
        body: Center(
            child: Container(
                height: 300,
                width: 300,
              
                decoration: BoxDecoration(
                  color: Colors.amber,
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    border: Border.all(color: Colors.black, width: 5),
                    gradient: const LinearGradient(
                      stops: [0.4,0.7],
                      colors: [Colors.red,Colors.amber],
                      begin: Alignment.topLeft,
                      end:Alignment.bottomRight,
                      
                    )
    ))));
  }
}
