import 'package:flutter/material.dart';

class Color_body extends StatelessWidget {
  const Color_body({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Hello Core2web"),
          backgroundColor: Colors.deepPurple,
          actions: [
            IconButton(icon: const Icon(Icons.search), onPressed: () {}),
            IconButton(
                icon: const Icon(Icons.favorite_rounded), onPressed: () {})
          ],
        ),
        body:  Center(
          child:
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 360,
                height: 200,
                color: Colors.amber,
              ),
              const SizedBox(
                width: 15
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.blue,
              ),
            ],
          ),
        )
    );

  }
}
