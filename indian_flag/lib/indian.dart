import 'package:flutter/material.dart';

class IndianFlag extends StatefulWidget {
  const IndianFlag({super.key});

  @override
  State<IndianFlag> createState() => _IndianFlagState();
}

class _IndianFlagState extends State<IndianFlag> {
  int num = -1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Republic Day",
          style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Color.fromARGB(235, 11, 3, 240),
              fontSize: 25),
        ),
        centerTitle: true,
        elevation: 30,
        shadowColor: Colors.black,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.orange, Colors.white, Colors.green],
              begin: Alignment.bottomLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      body: Container(
        color: const Color.fromARGB(255, 201, 221, 237),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                (num >= 0)
                    ? Container(
                        height: 600,
                        width: 20,
                        color: Colors.black,
                      )
                    : Container(),
                Column(
                  children: [
                    (num >= 1)
                        ? Container(
                            height: 75,
                            width: 300,
                            color: Colors.orange,
                          )
                        : Container(),
                    (num >= 2)
                        ? Container(
                            height: 75,
                            width: 300,
                            color: Colors.white,
                            child: (num >= 3)
                                ? Image.network(
                                    "https://www.shutterstock.com/image-vector/wheel-spokes-vector-iconmodern-ashok-600nw-2334393665.jpg")
                                : Container(),
                          )
                        : Container(),
                    (num >= 4)
                        ? Container(
                            height: 75,
                            width: 300,
                            color: Colors.green,
                          )
                        : Container(),
                  ],
                ),
              ],
            ),
            (num>=5)?
            Container(
              height: 30,
              width: 150,
              color: Colors.brown,
            )
            :Container(),
            (num>=6)?
            Container(
              height: 30,
              width: 200,
              color: Colors.brown,
            )
            :Container(),
            (num>=7)?
            Container(
              height: 30,
              width: 250,
              color: Colors.brown,
            )
            :Container(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            num++;
            print(num);
          });
        },
        backgroundColor: Colors.orange,
        child: const Text(
          "Next",
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}