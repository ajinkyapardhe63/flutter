import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Todolist extends StatefulWidget {
  const Todolist({super.key});
  @override
  State createState() => _todostate();
}


class _todostate extends State {
  int cardcnt = 0;

  List<Color> cardcolor = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Todolist"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.purple],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: ListView.builder(
            itemCount: cardcnt,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  height: 112,
                  width: 330,
                  decoration: BoxDecoration(
                      color: cardcolor[index],
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      boxShadow: const [
                        BoxShadow(
                            blurRadius: 8,
                            color: Color.fromARGB(255, 160, 160, 160),
                            offset: Offset(10, 10))
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Container(
                              height: 52,
                              width: 52,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                        color:
                                            Color.fromARGB(255, 206, 206, 206),
                                        blurRadius: (5))
                                  ]),
                              child: Image.asset('assets/img.png'),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Text("Lorem Ipsum is simply setting industry.",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black)),
                                const SizedBox(height: 10),
                                Text(
                                    "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500,
                                        color: const Color.fromARGB(
                                            255, 71, 71, 71)))
                              ],
                            ),
                          )
                          //const Expanded(child: Text("Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"))
                        ],
                      ),
                      Row(
                        children: [
                          const SizedBox(
                            width: 10,
                          ),
                          Text("10 July 2023",
                              style: GoogleFonts.quicksand(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: const Color.fromARGB(
                                      255, 158, 158, 149))),
                          const Spacer(),
                          const Icon(Icons.edit_outlined),
                          const SizedBox(width: 10),
                          const Icon(Icons.delete_outline)
                        ],
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              context: context,
              builder: (BuildContext context) {
                return Column(
                  children: [
                    const Text("create task"),
                    TextField(
                      decoration: InputDecoration(
                        hintText: "Enter Name :",
                        border: InputBorder.none,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Colors.blue,
                            width: 3,
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    ElevatedButton(
                      onPressed: () {
                        setState(() {
                          cardcnt++;

                          Navigator.of(context).pop();
                        });
                      },
                      child: const Text("Submit"),
                    )
                  ],
                );
              });
        },
        child: const Text("Add"),
      ),
    );
  }
}
