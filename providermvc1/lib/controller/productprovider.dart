import 'package:flutter/material.dart';

class ProductProvider with ChangeNotifier{
  String img;
  int productprice;
  String productname;
  ProductProvider({required this.img,required this.productname,required this.productprice});

  void changeText(String img,int productprice,String productname){
    this.img=img;
    this.productprice=productprice;
    this.productname=productname;
    notifyListeners();
  }
  
}