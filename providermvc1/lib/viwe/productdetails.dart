import 'package:flutter/material.dart';
import 'package:providermvc1/controller/productprovider.dart';
import'package:provider/provider.dart';

class ProductDetails extends StatefulWidget{
  const ProductDetails({super.key});
  @override
  State createState()=> _ProductDetails();
}
class _ProductDetails extends State{
  TextEditingController url=TextEditingController();
  TextEditingController pname=TextEditingController();
  TextEditingController pprice=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Product Details"),
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 39, 119, 42),
      ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: url,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Image URL",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                   Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: pname,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Product Name",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                   Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: pprice,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Product Name",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                   SizedBox(
                  height: 40,
                ),
                GestureDetector(
                  child: Container(
                    height: 50,
                    width: 159,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              const Color.fromARGB(255, 36, 153, 40),
                              Colors.black
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    child: const Center(
                        child: Text(
                      "Submit",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                  onTap: () {
                    Provider.of<ProductProvider>(context,listen:false).changeText(url.text,pprice as int,pname.text);
                  },
                ),
                Text(Provider.of<ProductProvider>(context).productname),
          ],
        ),
      ),
    );
  }
}