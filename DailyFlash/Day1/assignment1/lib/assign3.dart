import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Assign3 extends StatelessWidget {
  const Assign3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Appbar"),
        //without Container
        // backgroundColor:Colors.amber,               
        // shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft:Radius.circular(20),bottomRight: Radius.circular(20))),
        //with Container
        flexibleSpace: Container(
          decoration:const  BoxDecoration(
            gradient:LinearGradient(colors :[Colors.lightGreen,const Color.fromARGB(255, 6, 101, 9)],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter),
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
        ),
        
        ),
        
      )
    );
  }
}