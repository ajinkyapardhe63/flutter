import'package:flutter/material.dart';


class Assign2 extends StatelessWidget{
  const Assign2({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.arrow_back),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [Colors.blue,Color.fromARGB(255, 2, 41, 132)],
            begin: Alignment.topCenter,
            end:Alignment.bottomCenter,
            )
          ),
        ),
        actions: const [Icon(Icons.home,color: Colors.white,),Icon(Icons.favorite,color: Colors.white),Icon(Icons.person,color: Colors.white)],
      )
    );

  }
}