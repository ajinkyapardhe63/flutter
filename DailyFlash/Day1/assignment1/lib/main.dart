//Question - Create an AppBar, give an Icon at the start of the appbar, give a title
            //in the middle, and at the end add an Icon.

import 'package:flutter/material.dart';
import 'assign1.dart';


void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Appbar(),
    );
  }
}
