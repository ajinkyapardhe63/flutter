import'package:flutter/material.dart';
class Assign1 extends StatelessWidget{
  const Assign1({super.key});
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.arrow_back,color: Colors.white),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [Colors.red,Colors.purple],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            )
          ),
        ),
        centerTitle: true,
        title: const Text("AppBar",style: TextStyle(color: Colors.white),),
        actions:const  [Icon(Icons.home,color: Colors.white)],
      ),
    );
  }
}