import 'package:flutter/material.dart';

class Assign1 extends StatelessWidget{
  const  Assign1({super.key});
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body:Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius:const BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.red)
          ),
          child: Center(child:Text("This is a box")) 
        ),
      )
    );
  }
}