import 'package:flutter/material.dart';


class Assign5 extends StatefulWidget {
  const Assign5({super.key});
  @override
  State createState() => Assign5_State();
}

class Assign5_State extends State {
  bool val = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          child: (val == true)
              ? Container(
                  height: 200,
                  width: 200,
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 243, 174, 255),
                      border: Border.all(color: Colors.purple)),
                  child: const Center(child: Text("Click me")),
                )
              : Container(
                  height: 200,
                  width: 200,
                  decoration: BoxDecoration(
                      color:  const Color.fromARGB(255, 178, 220, 255),
                      border: Border.all(color: Colors.blue)),
                       child: const Center(child: Text("Container Tapped")),
                ),
                
          onTap: () => setState(() {
            val=!val;
          })
        ),
      ),
    );
  }
}
