import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Assign2 extends StatelessWidget {
  const Assign2({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
              color: Colors.purple,
              border: Border(left: BorderSide(width: 5, color: Colors.red))),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: const Text("this is a box"),
          )),
    ));
  }
}
