import 'package:flutter/material.dart';
import'assign1.dart';
import'assign2.dart';
import'assign3.dart';
import'assign4.dart';
import'assign5.dart';


void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:  Assign5(),
      debugShowCheckedModeBanner: false,
    );
  }

}
