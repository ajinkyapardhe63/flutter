import "package:flutter/material.dart";
import "package:flutter/widgets.dart";

class Assign4 extends StatelessWidget {
  const Assign4({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      height: 200,
      width: 300,
      decoration: BoxDecoration(color: Colors.amber, boxShadow: [
        BoxShadow(color: Colors.black, offset: Offset(0, -3), blurRadius: 2),
      ]),
    )));
  }
}
