import 'package:flutter/material.dart';



class Numbercount extends StatefulWidget {
  const Numbercount({Key? key}) : super(key: key);

  @override
  
  // ignore: library_private_types_in_public_api
  _NumbercountState createState() =>_NumbercountState();
}

class _NumbercountState extends State<Numbercount> {
  int _palindromeCount = 0;
  int _armstrongCount = 0;
  List<int> numbers = [121, 200, 323, 370, 371, 407];

  bool isPalindrome(int number) {
    int reverse = 0;
    int original = number;

    while (number > 0) {
      int digit = number % 10;
      reverse = reverse * 10 + digit;
      number = number ~/ 10;
    }

    return original == reverse;
  }

  bool isArmstrong(int number) {
    int sum = 0;
    int original = number;
    while (number > 0) {
      int digit = number % 10;
      sum += digit * digit * digit;
      number = number ~/ 10;
    }
    return sum == original;
  }

  void _incrementCounter() {
    int count = 0;
    for (int number in numbers) {
      if (isPalindrome(number)) {
        count++;
      }
    }
    setState(() {
      _palindromeCount = count;
    });
  }

  void _countArmstrong() {
    int count = 0;
    for (int number in numbers) {
      if (isArmstrong(number)) {
        count++;
      }
    }
    setState(() {
      _armstrongCount = count;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Number Counter'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text('Palindrome Count:',),
              Text('$_palindromeCount', style: Theme.of(context).textTheme.headlineMedium,),
              const SizedBox(height: 50,),
              ElevatedButton(
                onPressed: _incrementCounter,
                child: const Text('Count Palindromes'),
              ),
              const SizedBox(height: 50,),
              const Text('Armstrong Count:',),
              Text('$_armstrongCount', style: Theme.of(context).textTheme.headlineMedium,),
              const SizedBox(height: 50,),
              ElevatedButton(
                onPressed: _countArmstrong,
                child: const Text('Count Armstrongs'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}