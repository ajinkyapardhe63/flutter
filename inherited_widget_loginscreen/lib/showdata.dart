import"package:flutter/material.dart";
import'shareddata.dart';
class Showdata extends StatelessWidget{
  const Showdata({super.key});
  @override
  Widget build(BuildContext context) {
    Shareddata obj=Shareddata.of(context);
    return Scaffold(
      body: Center(
        
        child: Column(
          
          children: [
            SizedBox(height: 50),
            Text("ID : ${obj.id}"),
            SizedBox(height: 20),
            Text("Name : ${obj.name}",style: TextStyle(),),
            SizedBox(height: 20),
            Text("UserName: ${obj.username}"),

          ]
          
        ),
      ),
    );
  }
  
}