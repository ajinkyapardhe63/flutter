import 'package:flutter/material.dart';

class Shareddata extends InheritedWidget {
  final int id;
  final String name;
  final String username;
  const Shareddata(
      {super.key,
      required this.id,
      required this.name,
      required this.username,
      required super.child});
  static Shareddata of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Shareddata>()!;
  }

  @override
  bool updateShouldNotify(Shareddata oldWidget) {
    return (id != oldWidget.id) &&
        (name != oldWidget.name) &&
        (username != oldWidget.username);
  }
}
