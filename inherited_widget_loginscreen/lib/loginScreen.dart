import "package:flutter/material.dart";
import 'package:flutter/widgets.dart';
import 'package:inherited_widget_loginscreen/showdata.dart';
import 'shareddata.dart';

class Login extends StatefulWidget {
  const Login({super.key});
  @override
  State createState() => _Login();
}

class _Login extends State {
  TextEditingController userName = TextEditingController();
  TextEditingController empname = TextEditingController();
  TextEditingController id1 = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          appBar: AppBar(
            title: const Text("Login Screen"),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Center(
                  child: Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: id1,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "ID",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: empname,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "Name",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Center(
                  child: Container(
                    height: 49,
                    width: 320,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              offset: Offset(0, 3),
                              blurRadius: 10)
                        ]),
                    child: TextField(
                      controller: userName,
                      decoration: const InputDecoration(
                        hoverColor: Colors.white,
                        fillColor: Colors.white,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                        ),
                        hintText: "username",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Color.fromRGBO(0, 0, 0, 0.4)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                GestureDetector(
                  child: Container(
                    height: 50,
                    width: 159,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              const Color.fromARGB(255, 36, 153, 40),
                              Colors.black
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    child: const Center(
                        child: Text(
                      "Submit",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Shareddata(
                                name: empname.text,
                                id: int.parse(id1.text),
                                username: userName.text,
                                child: const Showdata())),
                      );
                    });
                  },
                )
              ],
            ),
          ),
        );
  }
}
