import 'package:flutter/material.dart';

class list extends StatefulWidget {
  const list({super.key});
  @override
  State createState() => _listState();
}

class _listState extends State {
  List<int> num = [];
  int count = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.purple, Colors.blue],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          )),
        ),
        centerTitle: true,
        title: const Text("Display numbers"),
        backgroundColor: Colors.purple,
      ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [Colors.purple, Colors.purple, Colors.blue],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        )),
        
        child: 
        ListView.builder(
            itemCount: num.length,
            itemBuilder: (context, index) {
              if (count > 1) {
                return Container(
                  margin: const EdgeInsets.all(10),
                  child: Text("${num[index]}"),
                );
              }
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            num.add(count);
            count++;
          });
        },
        child: const Text("Next"),
      ),
    );
  }
}
