
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import'package:flutter_svg/flutter_svg.dart';

class AdvanceUi extends StatefulWidget {
  const AdvanceUi({super.key});
  @override
  State createState() => AdvanceState();
}

class AdvanceState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: const Color.fromRGBO(205, 218, 218, 1),
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding:  const EdgeInsets.all(30),//bottom: MediaQuery.of(context).viewInsets.bottom,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 40),
                    const Row(
                      children: [
                        Icon(Icons.menu),
                        Spacer(),
                        Icon(Icons.notifications)
                      ],
                    ),
                    const SizedBox(height: 20),
                    Text(
                      "Welcome to New",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w300, fontSize: 26.98)),
                    ),
                    Text(
                      "Educourse",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 37)),
                    ),
                    const SizedBox(height: 10),
                    TextFormField(
                      decoration: InputDecoration(
                        suffixIcon: const Icon(Icons.search,size: 30,),
                        fillColor: Colors.white,
                        hintText: "   Enter Your Keyword",
                        hintStyle: const TextStyle(color: Color.fromRGBO(143, 143, 143, 1),fontSize: 14,fontWeight: FontWeight.w400),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(50),
                        ),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                          borderSide: const BorderSide(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40))),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(30),
                          child: Text("Course For You",
                              style: GoogleFonts.jost(
                                  textStyle: const TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18))),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              const SizedBox(
                                width: 30,
                              ),
                              GestureDetector(
                                child: Container(
                                  height: 242,
                                  width: 190,
                                  decoration: BoxDecoration(
                                      gradient: const LinearGradient(
                                          colors: [
                                            Color.fromRGBO(197, 4, 98, 1),
                                            Color.fromRGBO(80, 3, 112, 1)
                                          ],
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter),
                                      borderRadius: BorderRadius.circular(14)),
                                  child: Padding(
                                      padding: const EdgeInsets.all(20),
                                      child: Column(
                                        children: [
                                          Expanded(
                                              child: Text(
                                            "UX Designer from Scratch.",
                                            style: GoogleFonts.jost(
                                                textStyle: const TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 17,
                                                    color: Colors.white)),
                                          )),
                                          SvgPicture.asset("lib/assets/svgimg/img1.svg"),
                                        ],
                                      )),
                                ),
                                
                                onTap: () {
                                  // setState(() {
                                  //   print("clicked");
                                  // });
                                  //print("clicked");
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                          builder: ((context) =>
                                              const Screen1())));
                                },
                                
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              GestureDetector(
                                child: Container(
                                  height: 242,
                                  width: 190,
                                  decoration: BoxDecoration(
                                      gradient: const LinearGradient(
                                          colors: [
                                            Color.fromRGBO(0, 77, 228, 1),
                                            Color.fromRGBO(1, 47, 135, 1)
                                          ],
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter),
                                      borderRadius: BorderRadius.circular(14)),
                                  child: Padding(
                                      padding: const EdgeInsets.all(20),
                                      child: Column(
                                        children: [
                                          Expanded(
                                              child: Text(
                                            "Design Thinking The Beginner",
                                            style: GoogleFonts.jost(
                                                textStyle: const TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 17,
                                                    color: Colors.white)),
                                          )),
                                          SvgPicture.asset("lib/assets/svgimg/img2.svg"),
                                        ],
                                      )),
                    
                                ),
                                onTap: (){
                                   Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                          builder: ((context) =>
                                              const Screen2())));
                                },
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 10, left: 30, right: 30),
                          child: Text(
                            "Course By Category",
                            style: GoogleFonts.jost(
                                textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 18)),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 40,
                            top: 20,
                           
                          ),
                          child: Row(
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 36,
                                    width: 36,
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(25, 0, 56, 1),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: SvgPicture.asset("lib/assets/svgimg/img4.svg"),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "UI/UX",
                                    style: GoogleFonts.jost(
                                        textStyle: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  )
                                ],
                              ),
                              const SizedBox(
                                width: 54,
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: 36,
                                    width: 36,
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(25, 0, 56, 1),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: SvgPicture.asset("lib/assets/svgimg/img5.svg"),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "VISUAL",
                                    style: GoogleFonts.jost(
                                        textStyle: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  )
                                ],
                              ),
                              const SizedBox(
                                width: 34,
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: 36,
                                    width: 36,
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(25, 0, 56, 1),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: SvgPicture.asset("lib/assets/svgimg/img6.svg"),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "ILLUSTRATION",
                                    style: GoogleFonts.jost(
                                        textStyle: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  )
                                ],
                              ),
                              const SizedBox(
                                width: 31,
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: 36,
                                    width: 36,
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(25, 0, 56, 1),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: SvgPicture.asset("lib/assets/svgimg/img3.svg"),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "PHOTO",
                                    style: GoogleFonts.jost(
                                        textStyle: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}

class Screen1 extends StatefulWidget {
  const Screen1({super.key});
  @override
  State createState() => UiState1();
}

class UiState1 extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color.fromRGBO(197, 4, 98, 1),
              Color.fromRGBO(80, 3, 112, 1)
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0, 0.5]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 52, left: 26),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const AdvanceUi()));
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.white,
                ),
              )),
          Padding(
            padding:
                const EdgeInsets.only(top: 15, left: 50, right: 30, bottom: 30),
            child: Expanded(
              child: Column(children: [
                Text(
                  "UX Designer from Scratch.",
                  style: GoogleFonts.jost(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 32.61,
                          color: Colors.white)),
                ),
                const SizedBox(height: 10),
                Text(
                  "Basic guideline & tips & tricks for how to become a UX designer easily.",
                  style: GoogleFonts.jost(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: Color.fromRGBO(228, 205, 225, 1))),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(
                          gradient: const LinearGradient(colors:[Color.fromRGBO(0, 77, 228, 1),
                                          Color.fromRGBO(1, 47, 135, 1)],
                                          begin:Alignment.topCenter,
                                          end:Alignment.bottomCenter) ,
                          
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 1,
                              color: const Color.fromRGBO(255, 255, 255, 1))),
                      child: Icon(
                        Icons.person_2_outlined,
                        color: Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Author:",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color.fromRGBO(190, 154, 197, 1))),
                    ),
                    Text(
                      " Jenny",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white)),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Text(
                      "4.8",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white)),
                    ),
                    Icon(
                      Icons.star,
                      color: Color.fromRGBO(255, 146, 0, 1),
                      size: 18,
                    ),
                    Text(
                      "(20 reviwe)",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color.fromRGBO(190, 154, 197, 1))),
                    ),
                  ],
                ),
              ]),
            ),
          ),
          Expanded(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38)),
              ),
              child: ListView.builder(
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding:
                            const EdgeInsets.only(left:30, right: 30,bottom: 25),
                        child: Container(
                          height: 70,
                          width: 300,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 40,
                                    color: Color.fromRGBO(0, 0, 0, 0.15),
                                    offset: Offset(0, 8))
                              ]),
                          child: Padding(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: [
                                Container(
                                  height: 60,
                                  width: 46,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: SvgPicture.asset("lib/assets/svgimg/youtube.svg"),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                     const SizedBox(
                                  height: 10,
                                ),
                                    Text(
                                      "Introduction",
                                      style: GoogleFonts.jost(
                                          textStyle: const TextStyle(fontWeight: FontWeight.w500,fontSize: 17,color: Colors.black)),
                                    ),
                                    
                                    Text(
                                      "Lorem Ipsum is simply dummy text ... ",
                                      style: GoogleFonts.jost(
                                          textStyle: const TextStyle(fontWeight: FontWeight.w400,fontSize: 12,color: Color.fromRGBO(143, 143, 143, 1)))),
                                
                                  ],
                                )
                              ],
                            ),
                          ),
                        ));
                  }),
                  
            ),
          )
        ],
      ),
    ));
  }
}
class Screen2 extends StatefulWidget {
  const Screen2({super.key});
  @override
  State createState() => UiState2();
}

class UiState2 extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color.fromRGBO(0, 77, 228, 1),
              Color.fromRGBO(1, 47, 135, 1)
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0, 0.3]),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 52, left: 26),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const AdvanceUi()));
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.white,
                ),
              )),
          Padding(
            padding:
                const EdgeInsets.only(top: 15, left: 50, right: 30, bottom: 30),
            child: Expanded(
              child: Column(children: [
                Text(
                  "Design Thinking The Beginner.",
                  style: GoogleFonts.jost(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 32.61,
                          color: Colors.white)),
                ),
                const SizedBox(height: 10),
                Text(
                  "Basic guideline & tips & tricks for how to become a design thinker easily.",
                  style: GoogleFonts.jost(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: Color.fromRGBO(205, 212, 228, 1))),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Container(
                      height: 34,
                      width: 34,
                      decoration: BoxDecoration(
                          gradient: const LinearGradient(colors: [Color.fromRGBO(197, 4, 98, 1),Color.fromRGBO(80, 3, 112, 1)],
                          begin: Alignment.topCenter,
                          end:Alignment.bottomCenter),
                          
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 1,
                              color: const Color.fromRGBO(255, 255, 255, 1))),
                      child: const Icon(
                        Icons.person_2_outlined,
                        color: Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Author:",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color.fromRGBO(205, 212, 228, 1))),
                    ),
                    Text(
                      " Benny",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white)),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Text(
                      "4.8",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white)),
                    ),
                    Icon(
                      Icons.star,
                      color: Color.fromRGBO(255, 146, 0, 1),
                      size: 18,
                    ),
                    Text(
                      "(20 reviwe)",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              color: Color.fromRGBO(205, 212, 228, 1))),
                    ),
                  ],
                ),
              ]),
            ),
          ),
          Expanded(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38)),
              ),
              child: ListView.builder(
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return Padding(
                        padding:
                            const EdgeInsets.only(left: 30, bottom: 25, right: 30),
                        child: Container(
                          height: 70,
                          width: 300,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 40,
                                    color: Color.fromRGBO(0, 0, 0, 0.15),
                                    offset: Offset(0, 8))
                              ]),
                          child: Padding(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: [
                                Container(
                                  height: 60,
                                  width: 46,
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12))),
                                  child: Padding(padding:const EdgeInsets.all(10) ,child:SvgPicture.asset("lib/assets/svgimg/youtube.svg",)),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                     const SizedBox(
                                  height: 10,
                                ),
                                    Text(
                                      "Introduction",
                                      style: GoogleFonts.jost(
                                          textStyle: const TextStyle(fontWeight: FontWeight.w500,fontSize: 17,color: Colors.black)),
                                    ),
                                    
                                    Text(
                                      "Lorem Ipsum is simply dummy text ... ",
                                      style: GoogleFonts.jost(
                                          textStyle: const TextStyle(fontWeight: FontWeight.w400,fontSize: 12,color: Color.fromRGBO(143, 143, 143, 1)))),
                                
                                  ],
                                )
                              ],
                            ),
                          ),
                        ));
                  }),
                  
            ),
          )
        ],
      ),
    ));
  }
}

